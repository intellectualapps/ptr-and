package com.affinislabs.pentor.utils;

public class Constants {
    public static final String USER = "user";
    public static final String NOTIFICATION_USER_PARAM = "user-param";
    public static final String CONNECTION = "connection";
    public static final String LOCATION = "address";
    public static final String ABOUT = "short-bio";
    public static final String BIO = "long-bio";
    public static final String BIRTH_DAY = "birthday";
    public static final String IS_MENTOR = "is-mentor";
    public static final String EMAIL_ADDRESS = "email";
    public static final String REPORT_EMAIL_ADDRESS = "report-email";
    public static final String BLOCK_EMAIL_ADDRESS = "block-email";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String PHONE_NUMBER = "phone-number";
    public static final String USER_TYPE = "user-type";
    public static final String USER_EMAIL = "user-email";
    public static final String PASSWORD = "password";
    public static final String GENDER = "gender";
    public static final String PROFILE_PHOTO = "profile-photo";
    public static final String BASIC_PROFILE_DATA = "basic_profile_data";
    public static final String IMAGE_KEY = "image-type";
    public static final String IMAGE_DATA = "image-data";
    public static final String SOCIAL_PHOTO_URL = "social-photo-url";
    public static final String MENTOR_INTERESTS = "mentor-interests";
    public static final String LONGITUDE = "lon";
    public static final String LATITUDE = "lat";
    public static final String SUB_INTEREST_ID = "sub-interest-id";
    public static final String SOCIAL_PROFILE_DATA = "social_profile";
    public static final String MENTEE_EMAIL = "mentee-email";
    public static final String MENTOR_EMAIL = "mentor-email";
    public static final String INITIAL_MESSAGE = "initial-message";
    public static final String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "uuid";
    public static final String OFFLINE_EMAIL = "offline-email";
    public static final String ONLINE_EMAIL = "online-email";

    public static final String CATEGORIZED_KEY = "categorized";
    public static final String PHOTO_URL = "photo-url";
    public static final String NOTIFICATION_TYPE = "notification-type";
    public static final String NOTIFICATION_MESSAGE = "notification-message";

    public static final String VIEW_TYPE = "viewType";

    public static final String VERIFY_SMS_REQUEST = "VERIFY_ACCOUNT";
    public static final String RESEND_EMAIL_REQUEST = "RESEND_EMAIL";
    public static final String RESEND_SMS_REQUEST = "RESEND_SMS";
    public static final String REGISTER_REQUEST = "REGISTER";
    public static final String RESET_PASSWORD_REQUEST = "RESET_PASSWORD";
    public static final String UPDATE_PROFILE_REQUEST = "UPDATE_PROFILE";
    public static final String FETCH_USER_PROFILE_REQUEST = "FETCH_USER_PROFILE";
    public static final String LOGIN_REQUEST = "LOGIN";
    public static final String FETCH_INTERESTS_REQUEST = "FETCH_INTERESTS";
    public static final String FETCH_SUB_INTERESTS_REQUEST = "FETCH_SUB_INTERESTS";
    public static final String FETCH_MENTORS_REQUEST = "FETCH_MENTORS";
    public static final String FETCH_CONNECTIONS_REQUEST = "FETCH_CONNECTIONS";
    public static final String ESTABLISH_CONNECTION_REQUEST = "ESTABLISH_CONNECTION";
    public static final String FETCH_IMAGE_REQUEST = "FETCH_IMAGE";
    public static final String UPLOAD_FCM_TOKEN_REQUEST = "UPLOAD_FCM_TOKEN";
    public static final String LOAD_USER_REQUEST = "LOAD_USER_DATA";
    public static final String UPDATE_CONNECTION_STATUS_REQUEST = "UPDATE_CONNECTION_STATUS";
    public static final String SEND_MESSAGE_NOTIFICATION = "SEND_NOTIFICATION_REQUEST";


    public static final String WELCOME_VIEW_TAG = "WELCOME_VIEW";
    public static final String LOGIN_VIEW_TAG = "LOGIN_VIEW";
    public static final String BASIC_PROFILE_VIEW_TAG = "BASIC_PROFILE_VIEW";
    public static final String MENTOR_REQUEST_VIEW_TAG = "MENTOR_REQUEST_VIEW";
    public static final String MENTEE_REQUEST_VIEW_TAG = "MENTEE_REQUEST_VIEW";
    public static final String ACTIVE_CONNECTION_VIEW_TAG = "ACTIVE_CONNECTION_VIEW";
    public static final String CONNECTION_DECLINED_DIALOG = "CONNECTION_DECLINED_DIALOG";
    public static final String MENTOR_INTERESTS_VIEW_TAG = "MENTOR_INTERESTS_VIEW";
    public static final String MENTOR_LIST_VIEW_TAG = "MENTOR_LIST_VIEW";
    public static final String MENTOR_CONNECTIONS_VIEW_TAG = "MENTOR_CONNECTIONS_VIEW";
    public static final String CONNECTIONS_VIEW_TAG = "CONNECTIONS_VIEW";
    public static final String BIO_PROFILE_VIEW_TAG = "BIO_PROFILE_VIEW";
    public static final String REGISTRATION_VIEW_TAG = "REGISTRATION_VIEW";
    public static final String CREATE_PROFILE_TAG = "CREATE_PROFILE_VIEW";
    public static final String NOTIFICATION_VIEW_TAG = "NOTIFICATION_VIEW_TAG";


    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";
    public static final String MENTOR_MODE = "MENTOR_MODE";
    public static final String MENTEE_MODE = "MENTEE_MODE";

    public static final String DEVICE_PLATFORM = "1";
    public static final String DEVICE_PLATFORM_KEY = "platform";
    public static final String CONNECTION_REQUEST = "connection-request";
    public static final String CONNECTION_ACCEPTED = "connection-accepted";
    public static final String CONNECTION_DECLINED = "connection-declined";
    public static final String MESSAGE_NOTIFICATION = "offline-user";
    public static final String RATING = "rating";

    public static final CharSequence CONNECTION_REQUEST_NOTIFICATION_TITLE = "Connection Request";
    public static final CharSequence MESSAGE_NOTIFICATION_TITLE = "New Message";
    public static final String ACCEPT_STATUS_ID = "mn";
    public static final String DECLINE_STATUS_ID = "dl";
    public static final String ACTIVE_STATUS = "ACTIVE";
    public static final String DECLINED_STATUS = "DECLINED";
    public static final String ADMIN_TERMINATED_STATUS = "ADMIN_TERMINATED";
    public static final String PENDING_STATUS = "PENDING";
    public static final String MENTOR_KEY = "mentor";
    public static final String MENTEE_KEY = "mentee";
    public static final String CONNECTION_ID = "connection-id";

    public static final String CONNECTION_ID_KEY = "id";
    /* Firebase Section   */
    public static final String MESSAGE_ID_KEY = "id";
    public static final String MESSAGE_AUTHOR_KEY = "author";
    public static final String MESSAGE_TEXT_KEY = "text";

    public static final String MESSAGE_TIME_KEY = "time";
    public static final String CONNECTION_PARTICIPANTS_KEY = "participants";
    public static final String CONNECTION_MESSAGES_KEY = "messages";
    public static final String CONNECTIONS = "connections";
    public static final String PARTICIPANT_EMAIL_ADDRESS = "participant_email_address";
    public static final String ACTIVE_USER_EMAIL_ADDRESS = "active_user_email_address";

    public static final String HTML_LINE_BREAK = "<br/>";
}