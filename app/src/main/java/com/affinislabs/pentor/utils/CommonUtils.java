package com.affinislabs.pentor.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

public class CommonUtils {
    public static void displaySnackBarMessage(View mSnackBarAnchor, String message) {
        if (mSnackBarAnchor != null && mSnackBarAnchor.getContext() != null) {
            Snackbar.make(mSnackBarAnchor, message, Snackbar.LENGTH_SHORT).show();
        }
    }

    public static void displayToastMessage(Context context, String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
