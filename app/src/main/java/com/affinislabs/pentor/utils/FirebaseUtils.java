package com.affinislabs.pentor.utils;

import android.util.Log;

import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.ConnectionNode;
import com.affinislabs.pentor.models.Message;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FirebaseUtils {
    private static final String TAG = FirebaseUtils.class.getSimpleName();
    private static FirebaseDatabase firebaseDatabase;
    private static DatabaseReference firebaseDatabaseReference;
    private static DatabaseReference connectionsDatabaseReference;
    private static DatabaseReference connectionMessagesDatabaseReference;
    private static DatabaseReference connectionParticipantsDatabaseReference;

    public static void initializeFirebase() {
        if (firebaseDatabase == null) {
            firebaseDatabase = FirebaseDatabase.getInstance();
        }

        if (firebaseDatabaseReference == null) {
            firebaseDatabaseReference = firebaseDatabase.getReference();
        }

        if (connectionsDatabaseReference != null) {
            connectionsDatabaseReference = firebaseDatabaseReference.child(Constants.CONNECTIONS);
            connectionsDatabaseReference.keepSynced(true);
        }
    }

    public static void resetReferences() {
        firebaseDatabase = null;
        firebaseDatabaseReference = null;
        connectionParticipantsDatabaseReference = null;
        connectionMessagesDatabaseReference = null;
    }


    public static DatabaseReference getConnectionParticipantsDatabaseReference(String connectionId) {
        if (connectionParticipantsDatabaseReference == null) {
            connectionParticipantsDatabaseReference = getConnectionsDatabaseReference().child(connectionId).child(Constants.CONNECTION_PARTICIPANTS_KEY);
            connectionParticipantsDatabaseReference.keepSynced(true);
        }

        return connectionParticipantsDatabaseReference;
    }

    public static DatabaseReference getConnectionMessagesDatabaseReference(String connectionId) {
        connectionMessagesDatabaseReference = getConnectionsDatabaseReference().child(connectionId).child(Constants.CONNECTION_MESSAGES_KEY);
        connectionMessagesDatabaseReference.keepSynced(true);

        return connectionMessagesDatabaseReference;
    }

    public static DatabaseReference getFirebaseDatabaseReference() {
        if (firebaseDatabaseReference == null) {
            firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            firebaseDatabaseReference.keepSynced(true);
        }

        return firebaseDatabaseReference;
    }

    public static DatabaseReference getConnectionsDatabaseReference() {
        if (connectionsDatabaseReference == null) {
            connectionsDatabaseReference = getFirebaseDatabaseReference().child(Constants.CONNECTIONS);
            connectionsDatabaseReference.keepSynced(true);
        }

        return connectionsDatabaseReference;
    }

    public static Message getMessageFromMap(Map<String, Object> messageMap) {
        Message message = new Message();
        if (messageMap != null && messageMap.size() > 0) {
            message.setId((String) messageMap.get(Constants.MESSAGE_ID_KEY));
            message.setAuthor((String) messageMap.get(Constants.MESSAGE_AUTHOR_KEY));
            message.setText((String) messageMap.get(Constants.MESSAGE_TEXT_KEY));
            message.setTime((Long) messageMap.get(Constants.MESSAGE_TIME_KEY));
        }
        return message;
    }

    private static void setupConnectionNode(final String connectionId, String userEmailAddress, String participantEmailAddress, final PentorInterfaces.ConnectionDataListener connectionDataListener) {
        DatabaseReference connectionDatabaseReference = getConnectionsDatabaseReference().child(connectionId);
        Map<String, Object> connectionMap = new HashMap<String, Object>();
        connectionMap.put(Constants.CONNECTION_ID_KEY, connectionId);
        final Map<String, Object> participantDataMap = new HashMap<String, Object>();

        userEmailAddress = userEmailAddress.replace(".", "%");
        participantEmailAddress = participantEmailAddress.replace(".", "%");

        participantDataMap.put(userEmailAddress, true);
        participantDataMap.put(participantEmailAddress, false);

        connectionMap.put(Constants.CONNECTION_PARTICIPANTS_KEY, participantDataMap);

        connectionDatabaseReference.setValue(connectionMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.w(TAG, databaseReference.toString());
                    ConnectionNode connectionNode = new ConnectionNode();
                    connectionNode.setConnectionId(connectionId);
                    connectionNode.setParticipants(participantDataMap);
                    connectionDataListener.onConnectionDataLoaded(connectionNode);
                }
            }
        });
    }

    public static void setupOrLoadConnection(final String connectionId, final String userEmailAddress, final String participantEmailAddress, final PentorInterfaces.ConnectionDataListener connectionDataListener) {
        if (connectionId != null) {
            final DatabaseReference connectionNodeReference = getConnectionsDatabaseReference().child(connectionId);
            connectionNodeReference.child(Constants.CONNECTION_ID_KEY).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                        connectionNodeReference.child(Constants.CONNECTION_PARTICIPANTS_KEY).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {

                                    ConnectionNode connectionNode = new ConnectionNode();
                                    connectionNode.setConnectionId(connectionId);
                                    connectionNode.setParticipants((Map<String, Object>) dataSnapshot.getValue());
                                    connectionDataListener.onConnectionDataLoaded(connectionNode);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    } else {
                        setupConnectionNode(connectionId, userEmailAddress, participantEmailAddress, connectionDataListener);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
