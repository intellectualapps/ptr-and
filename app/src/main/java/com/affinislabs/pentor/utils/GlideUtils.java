package com.affinislabs.pentor.utils;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

public class GlideUtils {

    public static void loadGlideCircularImage(final ImageView targetImageView, final Context context, Object imagePath) {
        try {
            if (context != null) {
                Glide.with(context)
                        .load(imagePath)
                        .asBitmap()
                        //.centerCrop()
                        //.placeholder(R.drawable.ic_mentor)
                        .into(new BitmapImageViewTarget(targetImageView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                targetImageView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loadGlideImage(final ImageView targetImageView, final Context context, Object imagePath) {
        try {

            //Application Context (activity.getApplicationContext())
            if (context != null) {
                // && !((AppCompatActivity) context).isFinishing()) {
                Glide.with(context)
                        .load(imagePath)
                        .asBitmap()
                        .placeholder(new ColorDrawable(context.getResources().getColor(R.color.colorGray)))
                        .centerCrop()
                        .into(targetImageView);
            }

        } catch (Exception e) {
            Log.e("ConnectionAdapter", e.getMessage());
            Context applicationContext = PentorApplication.getAppInstance().getApplicationContext();
            Glide.with(applicationContext)
                    .load(imagePath)
                    .asBitmap()
                    .centerCrop()
                    .into(targetImageView);
        }
    }
}
