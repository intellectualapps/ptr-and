package com.affinislabs.pentor.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;

public class Validator {
    private static Context context = PentorApplication.getAppInstance().getApplicationContext();
    private static final String EDIT_TEXT_ERROR_TEXT = context.getString(R.string.edit_text_error);
    private static final String EDIT_TEXT_PHONE_ERROR_TEXT = context.getString(R.string.phone_number_validation_error);
    private static final String EDIT_TEXT_LENGTH = context.getString(R.string.edit_text_length_error);

    public Validator() {
    }

    private static boolean isInputEmpty(EditText editText) {
        if (editText == null) {
            return true;
        }

        String inputValue = editText.getText().toString().trim();

        return TextUtils.isEmpty(inputValue);
    }

    private static boolean isValidPhoneNumber(EditText editText) {
        if (editText == null) {
            return false;
        }

        String inputValue = editText.getText().toString().trim();
        return inputValue.length() == 11;
    }

    private static boolean isValidLength(EditText editText, int maxLength) {
        if (editText == null) {
            return false;
        }

        int inputValue = editText.getText().length();
        return inputValue <= maxLength;
    }

    public static boolean validateInputNotEmpty(EditText editText) {
        if (editText == null) return false;
        if (isInputEmpty(editText)) {
            editText.setError(EDIT_TEXT_ERROR_TEXT);
            editText.requestFocus();
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    public static boolean validateInputViewsNotEmpty(EditText[] editTextViews) {
        for (EditText editText : editTextViews) {
            if (editText == null) {
                return false;
            } else {
                if (isInputEmpty(editText)) {
                    editText.setError(EDIT_TEXT_ERROR_TEXT);
                    editText.requestFocus();
                    return false;
                } else {
                    editText.setError(null);
                }
            }
        }
        return true;
    }

    public static boolean validatePhoneNumber(EditText editText) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                editText.setError(EDIT_TEXT_ERROR_TEXT);
                editText.requestFocus();
                return false;
            } else {
                if (isValidPhoneNumber(editText)) {
                    editText.setError(null);
                } else {
                    editText.setError(EDIT_TEXT_PHONE_ERROR_TEXT);
                    editText.requestFocus();
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean validateInputLength(EditText editText, int length) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                editText.setError(EDIT_TEXT_ERROR_TEXT);
                editText.requestFocus();
                return false;
            } else {
                if (isValidLength(editText, length)) {
                    editText.setError(null);
                } else {
                    editText.setError(EDIT_TEXT_LENGTH);
                    editText.requestFocus();
                    return false;
                }
            }
        }

        return true;
    }

}