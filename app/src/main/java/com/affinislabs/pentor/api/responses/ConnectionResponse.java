package com.affinislabs.pentor.api.responses;

import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ConnectionResponse {


    @SerializedName("mentors")
    @Expose
    private ArrayList<Connection> mentors = null;
    @SerializedName("mentees")
    @Expose
    private ArrayList<Connection> mentees = null;
    @SerializedName("connections")
    @Expose
    private ArrayList<Connection> connections = null;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("developerMessage")
    @Expose
    private String developerMessage;

    public ArrayList<Connection> getConnections() {
        return connections;
    }

    public void setConnections(ArrayList<Connection> connections) {
        this.connections = connections;
    }

    public ArrayList<Connection> getMentors() {
        return mentors;
    }

    public void setMentors(ArrayList<Connection> mentors) {
        this.mentors = mentors;
    }

    public ArrayList<Connection> getMentees() {
        return mentees;
    }

    public void setMentees(ArrayList<Connection> mentees) {
        this.mentees = mentees;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }
}
