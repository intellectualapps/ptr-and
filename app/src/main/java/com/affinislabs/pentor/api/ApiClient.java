package com.affinislabs.pentor.api;

import android.os.AsyncTask;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.api.responses.ConnectionResponse;
import com.affinislabs.pentor.api.responses.DefaultResponse;
import com.affinislabs.pentor.api.responses.FCMTokenResponse;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.api.responses.InterestResponse;
import com.affinislabs.pentor.api.responses.LoginResponse;
import com.affinislabs.pentor.api.responses.MentorConnectionResponse;
import com.affinislabs.pentor.api.responses.MentorResponse;
import com.affinislabs.pentor.api.responses.ProfileUpdateResponse;
import com.affinislabs.pentor.api.responses.RegistrationResponse;
import com.affinislabs.pentor.api.responses.SubInterestResponse;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.utils.ApiUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.DebugUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiClient {
    public static class NetworkCallsRunner extends AsyncTask<Void, Void, Void> {
        private String imagekey, interestId, emailAddress, connectionId, connectionStatusId, offlineEmail, onlineEmail;
        private String REQUEST_TYPE;
        private Map<String, String> requestMap;
        private Map<String, RequestBody> partDataMap;

        //Listeners
        private ApiClientListener.AccountRegistrationListener mAccountRegistrationListener;
        private ApiClientListener.ProfileUpdatedListener mProfileUpdatedListener;
        private ApiClientListener.FetchInterestsListener mFetchInterestsListener;
        private ApiClientListener.FetchSubInterestsListener mFetchSubInterestsListener;
        private ApiClientListener.FetchMentorsListener mFetchMentorsListener;
        private ApiClientListener.FetchUserProfileListener mFetchUserProfileListener;
        private ApiClientListener.AccountAuthenticationListener mAccountAuthenticationListener;
        private ApiClientListener.FetchEncodedImageListener mFetchEncodedImageListener;
        private ApiClientListener.MentorConnectionListener mMentorConnectionListener;
        private ApiClientListener.FetchConnectionsListener mFetchConnectionsListener;
        private ApiClientListener.UpdateConnectionStatusListener mUpdateConnectionStatusListener;
        private ApiClientListener.UploadFCMTokenListener mUploadFCMTokenListener;
        private ApiClientListener.ResetPasswordListener mResetPasswordListener;

        //Responses
        private RegistrationResponse registrationResponse;
        private LoginResponse loginResponse;
        private ProfileUpdateResponse profileUpdateResponse;
        private UserProfileResponse userProfileResponse;
        private InterestResponse interestResponse;
        private SubInterestResponse subInterestResponse;
        private MentorResponse mentorResponse;
        private ConnectionResponse connectionResponse;
        private Connection connection;
        private ImageResponse imageResponse;
        private MentorConnectionResponse mentorConnectionResponse;
        private FCMTokenResponse fcmTokenResponse;

        private static ApiService apiService = null;

        public static void resetApiService() {
            apiService = null;
        }


        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.AccountRegistrationListener accountRegistrationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mAccountRegistrationListener = accountRegistrationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.AccountAuthenticationListener accountAuthenticationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mAccountAuthenticationListener = accountAuthenticationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.ProfileUpdatedListener profileUpdatedListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mProfileUpdatedListener = profileUpdatedListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.ResetPasswordListener resetPasswordListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mResetPasswordListener = resetPasswordListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String emailAddress, ApiClientListener.FetchUserProfileListener fetchUserProfileListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.emailAddress = emailAddress;
            this.mFetchUserProfileListener = fetchUserProfileListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String interestId, ApiClientListener.FetchSubInterestsListener fetchSubInterestsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.interestId = interestId;
            this.mFetchSubInterestsListener = fetchSubInterestsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, ApiClientListener.FetchInterestsListener fetchInterestsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.mFetchInterestsListener = fetchInterestsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> requestMap, ApiClientListener.FetchMentorsListener fetchMentorsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = requestMap;
            this.mFetchMentorsListener = fetchMentorsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String emailAddress, ApiClientListener.FetchConnectionsListener fetchConnectionsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.emailAddress = emailAddress;
            this.mFetchConnectionsListener = fetchConnectionsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String emailAddress, Map<String, String> requestMap, ApiClientListener.UploadFCMTokenListener uploadFCMTokenListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.emailAddress = emailAddress;
            this.requestMap = requestMap;
            this.mUploadFCMTokenListener = uploadFCMTokenListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> requestMap, ApiClientListener.MentorConnectionListener mentorConnectionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = requestMap;
            this.mMentorConnectionListener = mentorConnectionListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String connectionId, String connectionStatusId, ApiClientListener.UpdateConnectionStatusListener updateConnectionStatusListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.connectionId = connectionId;
            this.connectionStatusId = connectionStatusId;
            this.mUpdateConnectionStatusListener = updateConnectionStatusListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String imageKey, ApiClientListener.FetchEncodedImageListener fetchEncodedImageListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.imagekey = imageKey;
            this.mFetchEncodedImageListener = fetchEncodedImageListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> requestMap, ApiClientListener.FetchEncodedImageListener fetchEncodedImageListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = requestMap;
            this.mFetchEncodedImageListener = fetchEncodedImageListener;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (apiService == null) {
                apiService = ApiModule.getApiModuleInstance(PentorApplication.getCacheFile(), ApiUtils.buildHeaders(null)).getApiService();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    registrationResponse = registerUserAccount(requestMap);
                    break;
                case Constants.LOGIN_REQUEST:
                    loginResponse = authenticateUser(requestMap);
                    break;
                case Constants.UPDATE_PROFILE_REQUEST:
                    profileUpdateResponse = updateUserProfile(requestMap);
                    break;
                case Constants.RESET_PASSWORD_REQUEST:
                    userProfileResponse = resetPassword(requestMap);
                    break;
                case Constants.FETCH_USER_PROFILE_REQUEST:
                    userProfileResponse = fetchUserProfile(emailAddress);
                    break;
                case Constants.FETCH_INTERESTS_REQUEST:
                    interestResponse = fetchMentorInterests();
                    break;
                case Constants.FETCH_SUB_INTERESTS_REQUEST:
                    subInterestResponse = fetchSubInterests(interestId);
                    break;
                case Constants.FETCH_MENTORS_REQUEST:
                    mentorResponse = fetchMentors(requestMap);
                    break;
                case Constants.FETCH_CONNECTIONS_REQUEST:
                    connectionResponse = fetchConnections(emailAddress);
                    break;
                case Constants.FETCH_IMAGE_REQUEST:
                    imageResponse = fetchEncodedImage(imagekey);
                    break;
                case Constants.ESTABLISH_CONNECTION_REQUEST:
                    mentorConnectionResponse = sendMentorConnectionRequest(requestMap);
                    break;
                case Constants.UPDATE_CONNECTION_STATUS_REQUEST:
                    connection = updateConnectionStatus(connectionId, connectionStatusId);
                    break;
                case Constants.UPLOAD_FCM_TOKEN_REQUEST:
                    fcmTokenResponse = uploadFCMToken(emailAddress, requestMap);
                    break;
                case Constants.SEND_MESSAGE_NOTIFICATION:
                    sendMessageNotification(requestMap);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    mAccountRegistrationListener.onAccountRegistered(registrationResponse);
                    break;
                case Constants.LOGIN_REQUEST:
                    mAccountAuthenticationListener.onUserAuthenticated(loginResponse);
                    break;
                case Constants.UPDATE_PROFILE_REQUEST:
                    mProfileUpdatedListener.onProfileUpdated(profileUpdateResponse);
                    break;
                case Constants.RESET_PASSWORD_REQUEST:
                    mResetPasswordListener.onPasswordReset(userProfileResponse);
                    break;
                case Constants.FETCH_USER_PROFILE_REQUEST:
                    mFetchUserProfileListener.onProfileFetched(userProfileResponse);
                    break;
                case Constants.FETCH_INTERESTS_REQUEST:
                    mFetchInterestsListener.onInterestsFetched(interestResponse);
                    break;
                case Constants.FETCH_SUB_INTERESTS_REQUEST:
                    mFetchSubInterestsListener.onSubInterestsFetched(subInterestResponse);
                    break;
                case Constants.FETCH_MENTORS_REQUEST:
                    mFetchMentorsListener.onMentorsFetched(mentorResponse);
                    break;
                case Constants.FETCH_CONNECTIONS_REQUEST:
                    mFetchConnectionsListener.onConnectionsFetched(connectionResponse);
                    break;
                case Constants.FETCH_IMAGE_REQUEST:
                    mFetchEncodedImageListener.onEncodedImageFetched(imageResponse);
                    break;
                case Constants.ESTABLISH_CONNECTION_REQUEST:
                    mMentorConnectionListener.onConnectionEstablished(mentorConnectionResponse);
                    break;
                case Constants.UPDATE_CONNECTION_STATUS_REQUEST:
                    mUpdateConnectionStatusListener.onStatusUpdated(connection);
                    break;
                case Constants.UPLOAD_FCM_TOKEN_REQUEST:
                    mUploadFCMTokenListener.onFCMTokenUploaded(fcmTokenResponse);
                    break;
            }
        }

        private static RegistrationResponse registerUserAccount(Map<String, String> accountMap) {
            Call<RegistrationResponse> call;
            RegistrationResponse registrationResponse = null;

            try {
                call = apiService.registerAccount(accountMap);
                retrofit2.Response<RegistrationResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        registrationResponse = response.body();
                    } else {
                        Converter<ResponseBody, RegistrationResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(RegistrationResponse.class, new Annotation[0]);
                        try {
                            registrationResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return registrationResponse;
        }

        private static LoginResponse authenticateUser(Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse loginResponse = null;

            try {
                call = apiService.loginUser(accountMap);
                retrofit2.Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        loginResponse = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            loginResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return loginResponse;
        }

        private static ProfileUpdateResponse updateUserProfile(Map<String, String> accountMap) {
            Call<ProfileUpdateResponse> call;
            ProfileUpdateResponse profileUpdateResponse = null;

            try {
                String encodedImage = null;
                if (accountMap.containsKey(Constants.IMAGE_DATA)) {
                    encodedImage = accountMap.get(Constants.IMAGE_DATA);
                    accountMap.remove(Constants.IMAGE_DATA);
                }

                call = apiService.updateProfile(accountMap, encodedImage);
                retrofit2.Response<ProfileUpdateResponse> response = call.execute();

                if (response != null) {
                    if (response.isSuccessful()) {
                        profileUpdateResponse = response.body();
                    } else {
                        Converter<ResponseBody, ProfileUpdateResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(ProfileUpdateResponse.class, new Annotation[0]);
                        try {
                            profileUpdateResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return profileUpdateResponse;
        }

        private static UserProfileResponse resetPassword(Map<String, String> accountMap) {
            Call<UserProfileResponse> call;
            UserProfileResponse userProfileResponse = null;

            try {
                String encodedImage = null;
                if (accountMap.containsKey(Constants.IMAGE_DATA)) {
                    encodedImage = accountMap.get(Constants.IMAGE_DATA);
                    accountMap.remove(Constants.IMAGE_DATA);
                }

                call = apiService.resetPassword(accountMap);
                retrofit2.Response<UserProfileResponse> response = call.execute();

                if (response != null) {
                    if (response.isSuccessful()) {
                        userProfileResponse = response.body();
                    } else {
                        Converter<ResponseBody, UserProfileResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(UserProfileResponse.class, new Annotation[0]);
                        try {
                            userProfileResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return userProfileResponse;
        }

        private UserProfileResponse fetchUserProfile(String emailAddress) {
            Call<UserProfileResponse> call;
            UserProfileResponse userProfileResponse = null;

            try {
                call = apiService.fetchUserProfile(emailAddress);
                retrofit2.Response<UserProfileResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        userProfileResponse = response.body();
                    } else {
                        Converter<ResponseBody, UserProfileResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(UserProfileResponse.class, new Annotation[0]);
                        try {
                            userProfileResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return userProfileResponse;
        }

        private InterestResponse fetchMentorInterests() {
            Call<InterestResponse> call;
            InterestResponse interestResponse = null;

            try {
                call = apiService.fetchMentorInterests();
                retrofit2.Response<InterestResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        interestResponse = response.body();
                    } else {
                        Converter<ResponseBody, InterestResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(InterestResponse.class, new Annotation[0]);
                        try {
                            interestResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return interestResponse;
        }

        private ImageResponse fetchEncodedImage(String imageKey) {
            Call<ImageResponse> call;
            ImageResponse imageResponse = null;

            try {
                call = apiService.fetchImage(imageKey);
                retrofit2.Response<ImageResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        imageResponse = response.body();
                    } else {
                        Converter<ResponseBody, ImageResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(ImageResponse.class, new Annotation[0]);
                        try {
                            imageResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return imageResponse;
        }

        private MentorResponse fetchMentors(Map<String, String> dataMap) {
            Call<MentorResponse> call;
            MentorResponse mentorResponse = null;

            try {
                call = apiService.fetchMentors(dataMap);
                retrofit2.Response<MentorResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        mentorResponse = response.body();
                    } else {
                        Converter<ResponseBody, MentorResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(MentorResponse.class, new Annotation[0]);
                        try {
                            mentorResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mentorResponse;
        }

        private ConnectionResponse fetchConnections(String emailAddress) {
            Call<ConnectionResponse> call;
            ConnectionResponse connectionResponse = null;

            try {
                call = apiService.fetchConnections(emailAddress, Constants.CATEGORIZED_KEY);
                retrofit2.Response<ConnectionResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        connectionResponse = response.body();
                    } else {
                        Converter<ResponseBody, ConnectionResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(ConnectionResponse.class, new Annotation[0]);
                        try {
                            connectionResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return connectionResponse;
        }

        private Connection updateConnectionStatus(String connectionId, String connectionStatusId) {
            Call<Connection> call;
            Connection connection = null;

            try {
                call = apiService.updateConnectionStatus(connectionId, connectionStatusId);
                retrofit2.Response<Connection> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        connection = response.body();
                    } else {
                        Converter<ResponseBody, ConnectionResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(Connection.class, new Annotation[0]);
                        try {
                            connectionResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return connection;
        }

        private FCMTokenResponse uploadFCMToken(String emailAddress, Map<String, String> FCMToken) {
            Call<FCMTokenResponse> call;
            FCMTokenResponse fcmTokenResponse = null;

            try {
                if (apiService == null) {
                    apiService = ApiModule.getApiModuleInstance(PentorApplication.getCacheFile(), ApiUtils.buildHeaders(null)).getApiService();
                }

                call = apiService.sendFCMTokenToServer(emailAddress, requestMap);
                retrofit2.Response<FCMTokenResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        fcmTokenResponse = response.body();
                    } else {
                        Converter<ResponseBody, FCMTokenResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(FCMTokenResponse.class, new Annotation[0]);
                        try {
                            fcmTokenResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return fcmTokenResponse;
        }

        private MentorConnectionResponse sendMentorConnectionRequest(Map<String, String> dataMap) {
            Call<MentorConnectionResponse> call;
            MentorConnectionResponse mentorConnectionResponse = null;

            try {
                call = apiService.establishMentorConnection(dataMap);
                retrofit2.Response<MentorConnectionResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        mentorConnectionResponse = response.body();
                    } else {
                        Converter<ResponseBody, MentorConnectionResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(MentorConnectionResponse.class, new Annotation[0]);
                        try {
                            mentorConnectionResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mentorConnectionResponse;
        }

        private SubInterestResponse fetchSubInterests(String interestId) {
            Call<SubInterestResponse> call;
            SubInterestResponse subInterestResponse = null;

            try {
                call = apiService.fetchSubInterests(interestId);
                retrofit2.Response<SubInterestResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        subInterestResponse = response.body();
                    } else {
                        Converter<ResponseBody, SubInterestResponse> converter =
                                ApiModule.buildRetrofitAdapter(PentorApplication.getCacheFile(), null)
                                        .responseBodyConverter(SubInterestResponse.class, new Annotation[0]);
                        try {
                            subInterestResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return subInterestResponse;
        }

        public static void sendMessageNotification(Map<String, String> dataMap) {
            Call<DefaultResponse> call;
            call = apiService.sendMessageNotification(dataMap);
            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                }

                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {

                }
            });
        }

        public static void rateUser(Map<String, String> requestMap, ApiClientListener.RateUserListener rateUserListener) {
            Call<DefaultResponse> call;
            call = apiService.rateUser(requestMap);
            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                }

                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {

                }
            });
        }

        public static void blockUser(String userEmail, final String blockEmail, String connectionId, final ApiClientListener.BlockUserListener blockUserListener) {
            Call<Connection> call;
            Connection connection = null;

            call = apiService.blockUser(connectionId, blockEmail, userEmail);
            call.enqueue(new Callback<Connection>() {
                @Override
                public void onResponse(Call<Connection> call, Response<Connection> response) {
                    blockUserListener.onUserBlocked();
                }

                @Override
                public void onFailure(Call<Connection> call, Throwable t) {

                }
            });
        }

        public static void reportUser(String userEmail, String reportEmail,
                                      String connectionId, String messages, final ApiClientListener.ReportUserListener reportUserListener) {
            Call<Connection> call;
            Connection connection = null;

            call = apiService.reportUser(connectionId, reportEmail, userEmail, messages);
            call.enqueue(new Callback<Connection>() {
                @Override
                public void onResponse(Call<Connection> call, Response<Connection> response) {
                    reportUserListener.onUserReported();
                }

                @Override
                public void onFailure(Call<Connection> call, Throwable t) {

                }
            });
        }
    }
}