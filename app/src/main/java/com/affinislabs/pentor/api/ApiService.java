package com.affinislabs.pentor.api;

import com.affinislabs.pentor.api.responses.ConnectionResponse;
import com.affinislabs.pentor.api.responses.DefaultResponse;
import com.affinislabs.pentor.api.responses.FCMTokenResponse;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.api.responses.InterestResponse;
import com.affinislabs.pentor.api.responses.LoginResponse;
import com.affinislabs.pentor.api.responses.MentorConnectionResponse;
import com.affinislabs.pentor.api.responses.MentorResponse;
import com.affinislabs.pentor.api.responses.ProfileUpdateResponse;
import com.affinislabs.pentor.api.responses.RegistrationResponse;
import com.affinislabs.pentor.api.responses.SubInterestResponse;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.Connection;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


interface ApiService {
    @POST("/api/v1/user/device-token/{user-email}")
    Call<FCMTokenResponse> sendFCMTokenToServer(@Path("user-email") String emailAddress, @QueryMap Map<String, String> token);

    @POST("/api/v1/user")
    Call<RegistrationResponse> registerAccount(@QueryMap Map<String, String> userAccountMap);

    @POST("/api/v1/user/authenticate")
    Call<LoginResponse> loginUser(@QueryMap Map<String, String> userAccountMap);

    @POST("/api/v1/mentor/connection")
    Call<MentorConnectionResponse> establishMentorConnection(@QueryMap Map<String, String> dataMap);

    @FormUrlEncoded
    @PUT("/api/v1/user/basic")
    Call<ProfileUpdateResponse> updateProfile(@QueryMap Map<String, String> userAccountMap, @Field("image-data") String encodedImageValue);

    @GET("/api/v1/interest")
    Call<InterestResponse> fetchMentorInterests();

    @GET("/api/v1/interest/sub")
    Call<SubInterestResponse> fetchSubInterests(@Query("id") String id);

    @GET("/api/v1/user")
    Call<UserProfileResponse> fetchUserProfile(@Query("email") String email);

    @GET("/api/v1/mentor")
    Call<MentorResponse> fetchMentors(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/mentor/connection/{user-email}")
    Call<ConnectionResponse> fetchConnections(@Path("user-email") String emailAddress, @Query("connection-type") String connectionType);

    @PUT("/api/v1/mentor/connection/{connection-id}")
    Call<Connection> updateConnectionStatus(@Path("connection-id") String connectionId, @Query("status-id") String statusId);

    @GET("/image/{imageKey}")
    Call<ImageResponse> fetchImage(@Path("imageKey") String imageKey);

    @POST("/api/v1/mentor/connection/notify")
    Call<DefaultResponse> sendMessageNotification(@QueryMap Map<String, String> dataMap);

    @POST("/api/v1/user/rate")
    Call<DefaultResponse> rateUser(@QueryMap Map<String, String> dataMap);

    @POST("/api/v1/user/password")
    Call<UserProfileResponse> resetPassword(@QueryMap Map<String, String> dataMap);

    @POST("/api/v1/mentor/connection/block")
    Call<Connection> blockUser(@Query("connection-id") String connectionId, @Query("block-email") String blockEmail,
                               @Query("user-email") String userEmail);

    @POST("/api/v1/mentor/connection/report")
    Call<Connection> reportUser(@Query("connection-id") String connectionId, @Query("report-email") String reportEmail,
                               @Query("user-email") String userEmail, @Query("messages") String messages);


}
