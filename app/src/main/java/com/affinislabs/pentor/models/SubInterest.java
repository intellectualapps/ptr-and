package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubInterest implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subId")
    @Expose
    private String subId;
    @SerializedName("name")
    @Expose
    private String name;

    protected SubInterest(Parcel in) {
        id = in.readString();
        subId = in.readString();
        name = in.readString();
    }

    public static final Creator<SubInterest> CREATOR = new Creator<SubInterest>() {
        @Override
        public SubInterest createFromParcel(Parcel in) {
            return new SubInterest(in);
        }

        @Override
        public SubInterest[] newArray(int size) {
            return new SubInterest[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(subId);
        dest.writeString(name);
    }
}
