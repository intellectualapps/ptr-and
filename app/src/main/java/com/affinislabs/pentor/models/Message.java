package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

public class Message implements Parcelable, Comparable<Message> {
    private String id;
    private String author;
    private String text;
    private Long time;

    @Override
    public int compareTo(Message o) {
        if (getId() == null || o.getId() == null)
            return 0;
        return getId().compareTo(o.getId());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Message && ((Message) o).getId().equals(id);
    }

    public static Comparator<Message> COMPARE_BY_NAME = new Comparator<Message>() {
        public int compare(Message one, Message other) {
            return one.id.compareTo(other.id);
        }
    };

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public Message(Parcel in) {
        id = in.readString();
        author = in.readString();
        text = in.readString();
        time = in.readLong();
    }

    public Message() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(author);
        dest.writeString(text);
        dest.writeLong(time);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
