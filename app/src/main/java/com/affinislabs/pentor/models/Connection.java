package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.affinislabs.pentor.utils.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Connection implements Parcelable {
    @SerializedName("connectionId")
    @Expose
    private String connectionId;
    @SerializedName("subInterestId")
    @Expose
    private String subInterestId;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;

    @SerializedName("mentor")
    @Expose
    private User mentor = null;
    @SerializedName("mentee")
    @Expose
    private User mentee = null;
    private String mode = null;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("developerMessage")
    @Expose
    private String developerMessage;


    public Connection(String connectionId) {
        this.connectionId = connectionId;
    }

    private Connection(Parcel in) {
        connectionId = in.readString();
        subInterestId = in.readString();
        creationDate = in.readString();
        mode = in.readString();
        currentStatus = in.readString();
        mentor = (User) in.readParcelable(User.class.getClassLoader());
        mentee = (User) in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Connection> CREATOR = new Creator<Connection>() {
        @Override
        public Connection createFromParcel(Parcel in) {
            return new Connection(in);
        }

        @Override
        public Connection[] newArray(int size) {
            return new Connection[size];
        }
    };

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getSubInterestId() {
        return subInterestId;
    }

    public void setSubInterestId(String subInterestId) {
        this.subInterestId = subInterestId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public User getMentor() {
        return mentor;
    }

    public void setMentor(User mentor) {
        this.mentor = mentor;
        this.mode = Constants.MENTOR_MODE;
    }

    public User getMentee() {
        return mentee;
    }

    public void setMentee(User mentee) {
        this.mentee = mentee;
        this.mode = Constants.MENTEE_MODE;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(connectionId);
        dest.writeString(subInterestId);
        dest.writeString(creationDate);
        dest.writeString(mode);
        dest.writeString(currentStatus);
        dest.writeParcelable(mentor, 0);
        dest.writeParcelable(mentee, 0);
    }
}
