package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class UserRole implements Parcelable {

    @SerializedName("privileges")
    @Expose
    private List<String> privileges = null;
    @SerializedName("roleId")
    @Expose
    private String roleId;
    @SerializedName("description")
    @Expose
    private String description;


    protected UserRole(Parcel in) {
        privileges = in.createStringArrayList();
        roleId = in.readString();
        description = in.readString();
    }

    public static final Creator<UserRole> CREATOR = new Creator<UserRole>() {
        @Override
        public UserRole createFromParcel(Parcel in) {
            return new UserRole(in);
        }

        @Override
        public UserRole[] newArray(int size) {
            return new UserRole[size];
        }
    };

    public List<String> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<String> privileges) {
        this.privileges = privileges;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(privileges);
        dest.writeString(roleId);
        dest.writeString(description);
    }
}