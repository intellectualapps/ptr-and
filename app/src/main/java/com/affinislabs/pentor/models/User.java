package com.affinislabs.pentor.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class User implements Parcelable {
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("firstName")
    @Expose
    private String firstName = "";
    @SerializedName("lastName")
    @Expose
    private String lastName = "";
    @SerializedName("profilePhotoUrl")
    @Expose
    private String profilePhotoUrl = "";
    @SerializedName("longBio")
    @Expose
    private String longBio = "";
    @SerializedName("shortBio")
    @Expose
    private String shortBio = "";
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber = "";
    @SerializedName("authToken")
    @Expose
    private String authToken = "";
    @SerializedName("emailAddressVerfied")
    @Expose
    private Boolean isEmailAddressVerified = false;
    @SerializedName("phoneNumberVerified")
    @Expose
    private Boolean isPhoneNumberVerified = false;
    @SerializedName("userRoles")
    @Expose
    private ArrayList<UserRole> userRoles = null;
    @SerializedName("mentorInterests")
    @Expose
    private ArrayList<SubInterest> mentorInterests = null;
    @SerializedName("creationDate")
    @Expose
    private String creationDate = "";
    @SerializedName("birthday")
    @Expose
    private String birthday = "";
    @SerializedName("gender")
    @Expose
    private String gender = "";
    @SerializedName("isMentor")
    @Expose
    private boolean isMentor = false;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("address")
    @Expose
    private String address = "";

    public User() {
    }

    private User(Parcel in) {
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        profilePhotoUrl = in.readString();
        shortBio = in.readString();
        longBio = in.readString();
        phoneNumber = in.readString();
        authToken = in.readString();
        creationDate = in.readString();
        birthday = in.readString();
        gender = in.readString();
        address = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        isEmailAddressVerified = in.readByte() != 0;
        isPhoneNumberVerified = in.readByte() != 0;
        isMentor = in.readByte() != 0;
        userRoles = in.readArrayList(UserRole.class.getClassLoader());
        mentorInterests = in.readArrayList(SubInterest.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(email);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(profilePhotoUrl);
        parcel.writeString(shortBio);
        parcel.writeString(longBio);
        parcel.writeString(phoneNumber);
        parcel.writeString(authToken);
        parcel.writeString(creationDate);
        parcel.writeString(birthday);
        parcel.writeString(gender);
        parcel.writeString(address);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeByte((byte) (isEmailAddressVerified ? 1 : 0));
        parcel.writeByte((byte) (isPhoneNumberVerified ? 1 : 0));
        parcel.writeByte((byte) (isMentor ? 1 : 0));
        parcel.writeList(userRoles);
        parcel.writeList(mentorInterests);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getEmailAddressVerified() {
        return isEmailAddressVerified;
    }

    public void setEmailAddressVerified(Boolean emailAddressVerified) {
        isEmailAddressVerified = emailAddressVerified;
    }

    public Boolean getPhoneNumberVerified() {
        return isPhoneNumberVerified;
    }

    public void setPhoneNumberVerified(Boolean phoneNumberVerified) {
        isPhoneNumberVerified = phoneNumberVerified;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public ArrayList<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(ArrayList<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isMentor() {
        return isMentor;
    }

    public void setMentor(boolean mentor) {
        isMentor = mentor;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public ArrayList<SubInterest> getMentorInterests() {
        return mentorInterests;
    }

    public void setMentorInterests(ArrayList<SubInterest> mentorInterests) {
        this.mentorInterests = mentorInterests;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getLongBio() {
        return longBio;
    }

    public void setLongBio(String longBio) {
        this.longBio = longBio;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

