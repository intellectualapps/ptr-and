package com.affinislabs.pentor.fcm;

import android.util.Log;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.FCMTokenResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

public class InstanceIdListenerService extends FirebaseInstanceIdService {
    private static final String TAG = "FCM LISTENER";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        saveTokenToPrefs(refreshedToken);
    }

    private void saveTokenToPrefs(String token) {
        if (token != null) {
            PreferenceStorageManager.saveFCMToken(getApplicationContext(), token);
        }

        User user = PreferenceStorageManager.getUser(getApplicationContext());
        if (user != null && user.getEmail() != null) {
            Map<String, String> metaDataMap = new HashMap<String, String>();
            metaDataMap.put(Constants.FCM_TOKEN, PreferenceStorageManager.getFCMToken(PentorApplication.getAppInstance().getApplicationContext()));
            metaDataMap.put(Constants.DEVICE_ID, PreferenceStorageManager.getDeviceId(PentorApplication.getAppInstance().getApplicationContext()));
            metaDataMap.put(Constants.DEVICE_PLATFORM_KEY, Constants.DEVICE_PLATFORM);
            uploadFCMToken(user.getEmail(), metaDataMap);
        }
    }

    private void uploadFCMToken(String emailAddress, Map<String, String> metaDataMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPLOAD_FCM_TOKEN_REQUEST, emailAddress, metaDataMap, new ApiClientListener.UploadFCMTokenListener() {
            @Override
            public void onFCMTokenUploaded(FCMTokenResponse fcmTokenResponse) {
            }
        }).execute();
    }
}
