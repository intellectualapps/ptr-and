package com.affinislabs.pentor.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.ChatActivity;
import com.affinislabs.pentor.ui.activities.ConnectionsActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.NetworkUtils;

public class MenteeRequestFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = MenteeRequestFragment.class.getSimpleName();
    private Toolbar toolbar;
    private TextView mMenteeNameView, mMenteeShortBioView;
    private Button mAcceptRequestBtn, mDeclineRequestBtn;
    private ImageView mMenteeProfilePhoto;
    private View mSnackBarAnchor;
    private User user, mentor, mentee;
    private Connection connection;
    private String profileMessage, updateRequestMessage, connectionId;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MenteeRequestFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public MenteeRequestFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.MENTEE_KEY)) {
                mentee = getArguments().getParcelable(Constants.MENTEE_KEY);
            }

            if (getArguments().containsKey(Constants.MENTOR_KEY)) {
                mentor = getArguments().getParcelable(Constants.MENTOR_KEY);
            }

            if (getArguments().containsKey(Constants.CONNECTION)) {
                connection = getArguments().getParcelable(Constants.CONNECTION);
                connectionId = connection.getConnectionId();
            }
        }

        profileMessage = getString(R.string.fetching_profile_progress);
        updateRequestMessage = getString(R.string.updating_request_progress);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mentee_request, container, false);
        mSnackBarAnchor = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((ConnectionsActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.connections));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ConnectionsActivity) getActivity()).navigateUp();
            }
        });

        mMenteeNameView = (TextView) view.findViewById(R.id.mentee_name);
        mMenteeShortBioView = (TextView) view.findViewById(R.id.mentee_short_bio);
        mAcceptRequestBtn = (Button) view.findViewById(R.id.accept_request_btn);
        mDeclineRequestBtn = (Button) view.findViewById(R.id.decline_request_btn);
        mMenteeProfilePhoto = (ImageView) view.findViewById(R.id.mentee_profile_photo);

        mAcceptRequestBtn.setOnClickListener(this);
        mDeclineRequestBtn.setOnClickListener(this);

        loadMenteeData(mentee, connection);
        fetchUserProfile(mentee.getEmail());
    }

    private void loadMenteeData(User user, Connection connection) {
        if (user != null) {
            mMenteeNameView.setText(user.getFirstName().concat(" " + user.getLastName()));
            mMenteeShortBioView.setText(user.getShortBio());

            if (user.getProfilePhotoUrl() != null) {
                if (user.getProfilePhotoUrl().contains(getString(R.string.base_url))) {
                    String imageKey = user.getProfilePhotoUrl().split("image/")[1];
                    if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                        loadImageData(imageKey, mMenteeProfilePhoto);
                    } else {
                        fetchImageBackgroundTask(imageKey, mMenteeProfilePhoto, getContext());
                    }
                } else {
                    GlideUtils.loadGlideImage(mMenteeProfilePhoto, getContext(), user.getProfilePhotoUrl());
                }
            } else {
                GlideUtils.loadGlideImage(mMenteeProfilePhoto, getContext(), R.drawable.ic_mentor);
            }

        } else {
            closeFragment();
        }
    }

    private void fetchUserProfile(String emailAddress) {
        if (emailAddress != null && emailAddress.length() > 0) {
            if (NetworkUtils.isConnected(getContext())) {
                showLoadingIndicator(true, profileMessage);
                new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_PROFILE_REQUEST, emailAddress, new ApiClientListener.FetchUserProfileListener() {
                    @Override
                    public void onProfileFetched(UserProfileResponse userProfileResponse) {
                        showLoadingIndicator(false, profileMessage);
                        if (userProfileResponse != null && userProfileResponse.getMessage() == null) {
                            User user = extractUserDetails(userProfileResponse);
                            loadMenteeData(user, connection);
                        }
                    }
                }).execute();
            } else {
                CommonUtils.displaySnackBarMessage(mSnackBarAnchor, getString(R.string.network_connection_error));
            }
        } else {
            closeFragment();
        }
    }

    private void updateRequestConnectionStatus(final String connectionId, String statusId) {
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(true, updateRequestMessage);
            new ApiClient.NetworkCallsRunner(Constants.UPDATE_CONNECTION_STATUS_REQUEST, connectionId, statusId, new ApiClientListener.UpdateConnectionStatusListener() {
                @Override
                public void onStatusUpdated(Connection connection) {
                    showLoadingIndicator(false, updateRequestMessage);
                    if (connection != null && connection.getMessage() == null) {
                        switch (connection.getCurrentStatus()) {
                            case Constants.ACTIVE_STATUS:
                                Intent intent = new Intent(getActivity(), ChatActivity.class);
                                intent.putExtra(Constants.VIEW_TYPE, Constants.ACTIVE_CONNECTION_VIEW_TAG);
                                intent.putExtra(Constants.MENTEE_KEY, connection.getMentee());
                                intent.putExtra(Constants.MENTOR_KEY, connection.getMentor());
                                intent.putExtra(Constants.CONNECTION_ID, connection.getConnectionId());
                                if (ConnectionsFragment.refreshConnectionsListener != null) {
                                    ConnectionsFragment.refreshConnectionsListener.onConnectionRequestUpdated();
                                }
                                getActivity().finish();
                                startActivity(intent);
                                break;
                            case Constants.DECLINED_STATUS:
                                CommonUtils.displaySnackBarMessage(mSnackBarAnchor, getString(R.string.mentee_request_declined_message));
                                if (ConnectionsFragment.refreshConnectionsListener != null) {
                                    ConnectionsFragment.refreshConnectionsListener.onConnectionRequestUpdated();
                                }
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().finish();
                                    }
                                }, 2000);
                                break;
                        }
                    } else {
                        if (connection != null) {
                            CommonUtils.displaySnackBarMessage(mSnackBarAnchor, connection.getMessage());
                        }
                    }
                }
            }).execute();
        } else {
            CommonUtils.displaySnackBarMessage(mSnackBarAnchor, getString(R.string.network_connection_error));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accept_request_btn:
                updateRequestConnectionStatus(connection.getConnectionId(), Constants.ACCEPT_STATUS_ID);
                break;
            case R.id.decline_request_btn:
                updateRequestConnectionStatus(connection.getConnectionId(), Constants.DECLINE_STATUS_ID);
                break;
        }
    }
}
