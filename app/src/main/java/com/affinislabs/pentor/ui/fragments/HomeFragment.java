package com.affinislabs.pentor.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.activities.MentorActivity;
import com.affinislabs.pentor.ui.activities.ProfileActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;

public class HomeFragment extends BaseFragment implements View.OnClickListener, PentorInterfaces.UserProfileUpdateListener {
    private static final String TAG = HomeFragment.class.getSimpleName();
    private Toolbar toolbar;
    private TextView mFindMentorView, mMentorPhotoLabel;
    private ImageView mMentorPhoto;
    View mSnackBarAnchor;
    private User user;
    public static PentorInterfaces.UserProfileUpdateListener userProfileUpdateListener;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new HomeFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
        if (user == null)
            user = PreferenceStorageManager.getUser(getContext().getApplicationContext());

        userProfileUpdateListener = this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mSnackBarAnchor = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((HomeActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.home_label));
        mFindMentorView = (TextView) view.findViewById(R.id.find_mentor);
        mMentorPhotoLabel = (TextView) view.findViewById(R.id.mentor_photo_label);
        mMentorPhoto = (ImageView) view.findViewById(R.id.mentor_placeholder_photo);

        mFindMentorView.setOnClickListener(this);
        mMentorPhotoLabel.setOnClickListener(this);
        mMentorPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mentor_placeholder_photo:
            case R.id.mentor_photo_label:
            case R.id.find_mentor:
                Intent intent = new Intent(getContext(), MentorActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.MENTOR_INTERESTS_VIEW_TAG);
                intent.putExtra(Constants.USER, user);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onProfileUpdated(User userParam) {
        this.user = userParam;
    }
}
