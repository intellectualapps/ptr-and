package com.affinislabs.pentor.ui.fragments;

import android.content.Context;
import android.net.Uri;

import com.affinislabs.pentor.ui.dialogs.MediaPickerDialog;

public abstract class MediaPickerBaseFragment extends BaseFragment {

    MediaPickerDialog.MediaPickedListener mMediaPickedListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mMediaPickedListener = (MediaPickerDialog.MediaPickedListener) getActivity();
        } catch (ClassCastException cce) {
            throw new ClassCastException("The activity must implement " + MediaPickerDialog.MediaPickedListener.class.getSimpleName());
        }
    }

    /**
     * Abstract method for successful media pick
     *
     * @param fileUri
     */
    public abstract void onMediaPickerSuccess(Uri fileUri, int mediaType, String fragmentTag);

    /**
     * Abstract method for error media pick
     *
     * @param message
     */
    public abstract void onMediaPickerError(String message, String fragmentTag);
}
