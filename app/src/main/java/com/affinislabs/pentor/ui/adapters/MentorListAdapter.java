package com.affinislabs.pentor.ui.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.ListUtils;
import com.affinislabs.pentor.utils.NetworkUtils;

import java.util.ArrayList;


public class MentorListAdapter extends PagerAdapter implements View.OnClickListener {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<User> mentorList;
    private ViewPager mViewPager;
    private ProgressDialog progressDialog;

    private TextView mShortBioTextView;
    private TextView mMentorName;
    private ImageView mSkipMentorNavigator;
    private ImageView mAddMentorNavigator;
    private ImageView mMentorProfilePicture;
    View parent;
    private View mSnackBarAnchor;
    private int nextIndex = 0;
    private PentorInterfaces.MentorClickListener mentorClickListener;

    public MentorListAdapter(final Context context, ViewPager viewPager, PentorInterfaces.MentorClickListener mentorClickListener) {
        mContext = context;
        this.mentorList = new ArrayList<User>();
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mViewPager = viewPager;
        this.mentorClickListener = mentorClickListener;

    }

    public void setItems(ArrayList<User> mentors) {
        this.mentorList = mentors;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        int count = ListUtils.isEmpty(mentorList) ? 0 : mentorList.size();
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final User user = mentorList.get(position);

        View itemView = mLayoutInflater.inflate(R.layout.layout_find_mentor_result, container, false);
        mSnackBarAnchor = itemView;
        mMentorName = (TextView) itemView.findViewById(R.id.mentor_name);
        mShortBioTextView = (TextView) itemView.findViewById(R.id.mentor_short_bio);
        mMentorProfilePicture = (ImageView) itemView.findViewById(R.id.mentor_profile_photo);
        mSkipMentorNavigator = (ImageView) itemView.findViewById(R.id.skip_mentor);
        mAddMentorNavigator = (ImageView) itemView.findViewById(R.id.add_mentor);
        mSkipMentorNavigator.setOnClickListener(this);
        mAddMentorNavigator.setOnClickListener(this);

        if (user != null) {
            mMentorName.setText(user.getFirstName().concat(" ").concat(user.getLastName()));
            if (user.getProfilePhotoUrl() != null) {
                if (user.getProfilePhotoUrl().contains(mContext.getString(R.string.base_url))) {
                    String imageKey = user.getProfilePhotoUrl().split("image/")[1];

                    if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                        String imageData = FileUtils.getImageDataFromCache(imageKey);
                        try {
                            byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                            mMentorProfilePicture.setImageBitmap(decodedByte);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        fetchImageBackgroundTask(imageKey, mMentorProfilePicture, mContext);
                    }
                } else {
                    GlideUtils.loadGlideImage(mMentorProfilePicture, mContext, user.getProfilePhotoUrl());
                }
            } else {
                mMentorProfilePicture.setImageDrawable(null);
            }
            mShortBioTextView.setText(user.getShortBio());
        }

        container.addView(itemView);
        return itemView;
    }

    private void fetchImageBackgroundTask(final String imageKey, final ImageView profilePhoto, final Context context) {
        if (NetworkUtils.isConnected(context)) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        FileUtils.saveImageToCache(imageKey, imageResponse.getBase64ImageString());
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        profilePhoto.setImageBitmap(decodedByte);
                    }
                }
            }).execute();
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public void onClick(View v) {
        int totalItems = getCount();
        int currentIndex = mViewPager.getCurrentItem();
        User user = mentorList.get(currentIndex);
        switch (v.getId()) {
            case R.id.skip_mentor:
                currentIndex++;
                nextIndex = (currentIndex == totalItems) ? 0 : ++nextIndex;
                mViewPager.setCurrentItem(nextIndex);
                mentorClickListener.onMentorSkipClicked(user, currentIndex);
                break;
            case R.id.add_mentor:
                mentorClickListener.onMentorAddClicked(user, currentIndex);
                break;
        }
    }
}
