package com.affinislabs.pentor.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.utils.FontUtils;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * Created by Thundeyy on 03/02/2017.
 */

public class CustomTwitterLoginButton extends TwitterLoginButton {

    public CustomTwitterLoginButton(Context context) {
        super(context);
        init();
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (isInEditMode()) {
            return;
        }
        setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable
                .ic_signin_twitter), null, null, null);
        //setBackgroundResource(R.drawable.sign_up_button);
        //setTextSize(20);
        //setPadding(0, 0, 0, 0);
        //setPadding(30, 0, 10, 0);
        //setTextColor(getResources().getColor(R.color.tw__blue_default));
        setText(getContext().getString(R.string.twitter_signup_label));
        setTypeface(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD));
    }
}
