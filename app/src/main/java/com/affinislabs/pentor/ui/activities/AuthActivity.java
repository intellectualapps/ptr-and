package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.fragments.AuthFragment;
import com.affinislabs.pentor.ui.fragments.LoginFragment;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

public class AuthActivity extends BaseActivity {
    Bundle fragmentBundle;
    String viewType;
    User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            viewType = intent.getExtras().getString(Constants.VIEW_TYPE);
            user = intent.getExtras().getParcelable(Constants.USER);
            fragmentBundle = new Bundle();
        }

        if (PreferenceStorageManager.getLoggedInStatus(getApplicationContext())) {
            viewType = Constants.LOGIN_VIEW_TAG;
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.LOGIN_VIEW_TAG:
                        frag = LoginFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = AuthFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = AuthFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
        if (twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
        // Pass the activity result to the fragment, which will then pass the result to the login
        // button.
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AuthFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();//((GroupActivity) getActivity()).navigateUp();
        }
    }
}
