package com.affinislabs.pentor.ui.fragments;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.ConnectionNode;
import com.affinislabs.pentor.models.Message;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.ChatActivity;
import com.affinislabs.pentor.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.pentor.ui.adapters.MessagesAdapter;
import com.affinislabs.pentor.ui.customviews.CustomRecyclerView;
import com.affinislabs.pentor.ui.dialogs.BlockConnectionDialog;
import com.affinislabs.pentor.ui.dialogs.EndConnectionDialog;
import com.affinislabs.pentor.ui.dialogs.ReportConnectionDialog;
import com.affinislabs.pentor.ui.dialogs.SocialPhotoDialog;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.DateUtils;
import com.affinislabs.pentor.utils.FirebaseUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ActiveConnectionFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, ChildEventListener, ValueEventListener, TextWatcher, PentorInterfaces.MessagePaginationListener, PentorInterfaces.ConnectionDataListener {
    private static final String TAG = ActiveConnectionFragment.class.getSimpleName();
    private Toolbar toolbar;
    private View mSnackBarView;
    private User mentor, mentee, user;
    private Connection connection;
    private MessagesAdapter mAdapter;
    private CustomRecyclerView messagesRecyclerView;
    private EditText composeMessageInput;
    private ImageButton sendMessageButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyView;
    private String connectionId, activeUserEmailAddress, participantEmailAddress;
    private ArrayList<Message> messages;
    private DatabaseReference connectionMessagesDatabaseReference;
    private boolean messagesLoadedOnce = false, isPartnerOnline = false, loadMoreMode = false;
    public static int INITIAL_MESSAGE_LOAD_COUNT = 100;
    private boolean isConnectionEnded = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ActiveConnectionFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public ActiveConnectionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        user = PreferenceStorageManager.getUser(PentorApplication.getAppInstance().getApplicationContext());
        activeUserEmailAddress = user.getEmail();

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.MENTOR_KEY)) {
                mentor = getArguments().getParcelable(Constants.MENTOR_KEY);
                if (mentor.getEmail() != null && !mentor.getEmail().equalsIgnoreCase(activeUserEmailAddress)) {
                    participantEmailAddress = mentor.getEmail();
                }
            }

            if (getArguments().containsKey(Constants.MENTEE_KEY)) {
                mentee = getArguments().getParcelable(Constants.MENTEE_KEY);
                if (mentee.getEmail() != null && !mentee.getEmail().equalsIgnoreCase(activeUserEmailAddress)) {
                    participantEmailAddress = mentee.getEmail();
                }
            }

            if (getArguments().containsKey(Constants.CONNECTION_ID)) {
                connectionId = getArguments().getString(Constants.CONNECTION_ID);
            }

            if (getArguments().containsKey(Constants.CONNECTION)) {
                connection = getArguments().getParcelable(Constants.CONNECTION);
            }
        }

        messages = new ArrayList<Message>();
        connectionMessagesDatabaseReference = FirebaseUtils.getConnectionMessagesDatabaseReference(connectionId);
        Log.w(TAG, connectionMessagesDatabaseReference.toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_connection, container, false);
        mSnackBarView = view;
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.active_connection_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                Bundle args = new Bundle();
                args.putString(Constants.CONNECTION_ID, connectionId);
                args.putString(Constants.EMAIL_ADDRESS, participantEmailAddress);
                DialogFragment endConnectionDialog = EndConnectionDialog.getInstance(args);
                if (getActivity() != null)
                    endConnectionDialog.show(getActivity().getSupportFragmentManager(), endConnectionDialog.getClass().getSimpleName());
                break;
            case R.id.action_block:
                Bundle blockArgs = new Bundle();
                blockArgs.putString(Constants.CONNECTION_ID, connectionId);
                blockArgs.putString(Constants.BLOCK_EMAIL_ADDRESS, participantEmailAddress);
                blockArgs.putString(Constants.USER_EMAIL, activeUserEmailAddress);
                DialogFragment blockConnectionDialog = BlockConnectionDialog.getInstance(blockArgs);
                if (getActivity() != null)
                    blockConnectionDialog.show(getActivity().getSupportFragmentManager(), blockConnectionDialog.getClass().getSimpleName());
                break;
            case R.id.action_report:
                Bundle reportArgs = new Bundle();
                reportArgs.putString(Constants.CONNECTION_ID, connectionId);
                reportArgs.putString(Constants.REPORT_EMAIL_ADDRESS, participantEmailAddress);
                reportArgs.putString(Constants.USER_EMAIL, activeUserEmailAddress);
                if (!messages.isEmpty()) {
                    String parsedMessages = parseRecentMesssage(messages);
                    reportArgs.putString(Constants.CONNECTION_MESSAGES_KEY, parsedMessages);
                }
                DialogFragment reportConnectionDialog = ReportConnectionDialog.getInstance(reportArgs);
                if (getActivity() != null)
                    reportConnectionDialog.show(getActivity().getSupportFragmentManager(), reportConnectionDialog.getClass().getSimpleName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((ChatActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.conversation_view_tag));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


        composeMessageInput = (EditText) view.findViewById(R.id.compose_text_input);
        sendMessageButton = (ImageButton) view.findViewById(R.id.send_chat_button);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mEmptyView = view.findViewById(R.id.empty_view);
        messagesRecyclerView = (CustomRecyclerView) view.findViewById(R.id.messages_recyclerview);

        composeMessageInput.addTextChangedListener(this);
        sendMessageButton.setOnClickListener(this);

        mAdapter = new MessagesAdapter(getContext(), this, this, user.getEmail());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        messagesRecyclerView.setLayoutManager(mLayoutManager);
        messagesRecyclerView.setAdapter(mAdapter);
        messagesRecyclerView.setEmptyView(mEmptyView);
        mAdapter.setItems(messages);

        mSwipeRefreshLayout.setColorSchemeColors(
                getActivity().getResources().getColor(R.color.orange),
                getActivity().getResources().getColor(R.color.green),
                getActivity().getResources().getColor(R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    loadConversations();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        messagesRecyclerView.setOnScrollListener(
                new android.support.v7.widget.RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(android.support.v7.widget.RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(android.support.v7.widget.RecyclerView recyclerView, int dx, int dy) {
                        int topRowVerticalPosition =
                                (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                        mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                    }
                }
        );

        if (mentor == null && mentee == null && connectionId == null && user == null) {
            closeFragment();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeData();
        if (!isConnectionEnded) {
            FirebaseUtils.setupOrLoadConnection(connectionId, activeUserEmailAddress, participantEmailAddress, this);
            updateOnlineStatus(connectionId, activeUserEmailAddress, true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isConnectionEnded) {
            updateOnlineStatus(connectionId, activeUserEmailAddress, false);
        }
        connectionMessagesDatabaseReference.removeEventListener((ChildEventListener) this);
        connectionMessagesDatabaseReference.removeEventListener((ValueEventListener) this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isConnectionEnded) {
            updateOnlineStatus(connectionId, activeUserEmailAddress, false);
        }
        connectionMessagesDatabaseReference.removeEventListener((ChildEventListener) this);
        connectionMessagesDatabaseReference.removeEventListener((ValueEventListener) this);
    }

    void initializeData() {
        animateSwipeRefreshLayout(mSwipeRefreshLayout);
        loadConversations();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_chat_button:
                if (NetworkUtils.isConnected(getContext())) {
                    if (composeMessageInput.getText().toString().trim().length() > 0) {
                        final String messageToSend = composeMessageInput.getText().toString().trim();
                        uploadMessage(messageToSend);
                    } else {
                        CommonUtils.displayToastMessage(getContext(), getString(R.string.blank_message_label));
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_error));
                }
                break;
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0 && !Objects.equals(s.toString().trim(), "")) {
            adjustSendButtonState(true);
        } else {
            adjustSendButtonState(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void loadConversationAdapter(ArrayList<Message> messages) {
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.setItems(messages);
    }

    private void adjustSendButtonState(boolean stateFlag) {
        if (getContext() != null) {
            if (stateFlag) {
                sendMessageButton.setEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sendMessageButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDarkOrange), PorterDuff.Mode.SRC_IN);
                } else {
                    Drawable wrapDrawable = DrawableCompat.wrap(sendMessageButton.getDrawable());
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDarkOrange));
                    sendMessageButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                }
            } else {
                sendMessageButton.setEnabled(false);
                if (getContext() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        sendMessageButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDark), PorterDuff.Mode.SRC_IN);
                    } else {
                        Drawable wrapDrawable = DrawableCompat.wrap(sendMessageButton.getDrawable());
                        DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDark));
                        sendMessageButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                    }
                }
            }
        }
    }

    private void uploadMessage(String messageToSend) {
        if (messageToSend != null && messageToSend.length() > 0) {
            adjustSendButtonState(false);
            Map<String, Object> newMessageMap = new HashMap<>();
            Calendar calendar = GregorianCalendar.getInstance();
            long timestamp = calendar.getTimeInMillis();

            newMessageMap.put(Constants.MESSAGE_TEXT_KEY, messageToSend);
            newMessageMap.put(Constants.MESSAGE_AUTHOR_KEY, user.getEmail());
            newMessageMap.put(Constants.MESSAGE_TIME_KEY, timestamp);

            final String autoGeneratedMessageKey = connectionMessagesDatabaseReference.push().getKey();
            DatabaseReference newMessageReference = connectionMessagesDatabaseReference.child(autoGeneratedMessageKey);
            newMessageMap.put(Constants.MESSAGE_ID_KEY, autoGeneratedMessageKey);

            newMessageReference.setValue(newMessageMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                }
            });

            composeMessageInput.setText(null);
            if (!isPartnerOnline) {
                Map<String, String> notificationMap = new HashMap<>();
                notificationMap.put(Constants.CONNECTION_ID, connectionId);
                notificationMap.put(Constants.OFFLINE_EMAIL, participantEmailAddress);
                notificationMap.put(Constants.ONLINE_EMAIL, activeUserEmailAddress);

                ApiClient.NetworkCallsRunner.sendMessageNotification(notificationMap);
            }
        } else {
            CommonUtils.displayToastMessage(getContext(), getString(R.string.blank_message_label));
        }
    }

    private void loadConversations() {
        connectionMessagesDatabaseReference.removeEventListener((ChildEventListener) this);
        connectionMessagesDatabaseReference.orderByKey().limitToLast(INITIAL_MESSAGE_LOAD_COUNT).addChildEventListener(this);
    }

    @Override
    public void onLoadMore(int position, String messageKey) {
        Query query = connectionMessagesDatabaseReference.orderByKey().limitToFirst(50).endAt(messageKey);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);

        ArrayList<Message> loadedMessages = new ArrayList<Message>();
        if (dataSnapshot != null && dataSnapshot.exists() && dataSnapshot.getValue() != null) {
            System.out.println(dataSnapshot.getChildrenCount() + " messages");

            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                Message message = FirebaseUtils.getMessageFromMap((Map<String, Object>) messageSnapshot.getValue());
                if (!messages.contains(message)) {
                    loadedMessages.add(message);
                }
            }

            messages.addAll(0, loadedMessages);
            messagesRecyclerView.smoothScrollToPosition(0);
        }
        connectionMessagesDatabaseReference.removeEventListener((ValueEventListener) this);
    }

    @Override
    public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
        if (dataSnapshot.exists() && dataSnapshot.hasChildren()) {
            new Runnable() {
                @Override
                public void run() {
                    if (mSwipeRefreshLayout.isRefreshing())
                        mSwipeRefreshLayout.setRefreshing(false);

                    Message message = FirebaseUtils.getMessageFromMap((Map<String, Object>) dataSnapshot.getValue());
                    if (!messages.contains(message)) {
                        messages.add(message);
                        mAdapter.notifyDataSetChanged();
                        messagesRecyclerView.smoothScrollToPosition(messages.size() - 1);
                    }
                }
            }.run();

        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    @Override
    public void onConnectionDataLoaded(ConnectionNode connectionNode) {
        Map<String, Object> map = connectionNode.getParticipantData();
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String emailAddress = entry.getKey().replace("%", ".");
                Boolean isUserOnline = false;

                if (entry.getValue().getClass() == Boolean.class) {
                    isUserOnline = (Boolean) entry.getValue();
                } else {
                    FirebaseUtils.getConnectionParticipantsDatabaseReference(connectionId).child(entry.getKey()).removeValue();
                    emailAddress = (String) entry.getValue();
                    isUserOnline = false;
                }

                if (!user.getEmail().equalsIgnoreCase(emailAddress)) {
                    participantEmailAddress = emailAddress;
                    isPartnerOnline = isUserOnline;
                } else {
                    activeUserEmailAddress = emailAddress;
                }
            }
        }

        FirebaseUtils.getConnectionParticipantsDatabaseReference(connectionId).child(participantEmailAddress.replace(".", "%")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.hasChildren() && dataSnapshot.getValue() != null) {
                    isPartnerOnline = (Boolean) dataSnapshot.getValue();
                } else {
                    isPartnerOnline = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateOnlineStatus(String connectionId, String emailAddress, boolean status) {
        DatabaseReference connectionDatabaseReference = FirebaseUtils.getConnectionsDatabaseReference().child(connectionId);
        connectionDatabaseReference.child(Constants.CONNECTION_PARTICIPANTS_KEY).child(emailAddress.replace(".", "%")).setValue(status, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

            }
        });
    }

    private String parseRecentMesssage(List<Message> recentMessages) {

        StringBuilder parsedMessages = new StringBuilder();
        for (Message message : recentMessages) {
            parsedMessages.append(message.getAuthor());
            parsedMessages.append(" ");
            parsedMessages.append(DateUtils.getSimpleDateFormat(message.getTime(), "dd MMMM, yyyy"));
            parsedMessages.append(Constants.HTML_LINE_BREAK);
            parsedMessages.append(message.getText());
            parsedMessages.append(Constants.HTML_LINE_BREAK);
            parsedMessages.append(Constants.HTML_LINE_BREAK);
        }

        return parsedMessages.toString();
    }

}
