package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.affinislabs.pentor.R;

public class TempActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        Intent intent = new Intent(TempActivity.this, AuthActivity.class);
        startActivity(intent);
        finish();
    }
}
