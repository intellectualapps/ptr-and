package com.affinislabs.pentor.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.ApiModule;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.customviews.CustomTypefaceSpan;
import com.affinislabs.pentor.ui.fragments.ConnectionsFragment;
import com.affinislabs.pentor.ui.fragments.CreateProfileFragment;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.DebugUtils;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.FontUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;

import java.io.File;
import java.util.Objects;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, PentorInterfaces.ChangeHomeActivityViewListener, PentorInterfaces.UserProfileUpdateListener {

    private User user, notificationUserParam;
    private int mCurrentTabIndex = 0;
    private static final String TAG = HomeActivity.class.getSimpleName();

    private Bundle fragmentBundle = new Bundle();
    private String viewType, notificationViewType, connectionId, notificationMessage;
    private boolean isPushNotificationMode = false;
    private Toolbar toolbar;
    private ImageView profilePhoto;
    private TextView mUsername;
    public static PentorInterfaces.ChangeHomeActivityViewListener changeHomeActivityViewListener;
    public static PentorInterfaces.UserProfileUpdateListener userProfileUpdateListener;
    private View mSnackBarAnchor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        changeHomeActivityViewListener = this;

        userProfileUpdateListener = this;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);

        mUsername = (TextView) headerLayout.findViewById(R.id.username);
        profilePhoto = (ImageView) headerLayout.findViewById(R.id.user_profile_photo);

        styleNavigationMenu(navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            user = intent.getExtras().getParcelable(Constants.USER);
            fragmentBundle.putParcelable(Constants.USER, user);
            notificationUserParam = intent.getExtras().getParcelable(Constants.NOTIFICATION_USER_PARAM);
            notificationViewType = intent.getExtras().getString(Constants.NOTIFICATION_VIEW_TAG);
            connectionId = intent.getExtras().getString(Constants.CONNECTION_ID);
            notificationMessage = intent.getExtras().getString(Constants.NOTIFICATION_MESSAGE);
            isPushNotificationMode = intent.getExtras().getBoolean(Constants.NOTIFICATION_FLAG);
        }

        if (user == null) {
            user = PreferenceStorageManager.getUser(getApplicationContext());
        }

        if (user == null) {
            return;
        }

        if (Objects.equals(user.getFirstName(), "") || Objects.equals(user.getLastName(), "")) {
            viewType = Constants.CREATE_PROFILE_TAG;
        } else {
            viewType = Constants.CONNECTIONS_VIEW_TAG;
        }

        initializeFragment(viewType, savedInstanceState);
        mSnackBarAnchor = getCurrentFocus();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState != null && savedInstanceState.containsKey(Constants.VIEW_TYPE)) {
            viewType = savedInstanceState.getString(Constants.VIEW_TYPE);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.VIEW_TYPE, viewType);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onResume() {
        super.onResume();

        loadUserData(user);

        if (isPushNotificationMode) {
            resolveNotificationView();
        }
    }

    private void resolveNotificationView() {
        Connection connection = new Connection(connectionId);
        switch (notificationViewType) {
            default:
            case Constants.MENTEE_REQUEST_VIEW_TAG:
                DebugUtils.debugObject(notificationUserParam);
                Intent launchProfileIntent = new Intent(HomeActivity.this, ConnectionsActivity.class);
                launchProfileIntent.putExtra(Constants.MENTEE_KEY, notificationUserParam);
                launchProfileIntent.putExtra(Constants.VIEW_TYPE, Constants.MENTEE_REQUEST_VIEW_TAG);
                launchProfileIntent.putExtra(Constants.CONNECTION, connection);
                launchProfileIntent.putExtra(Constants.CONNECTION_ID, connectionId);
                startActivity(launchProfileIntent);
                break;
            case Constants.ACTIVE_CONNECTION_VIEW_TAG:
                Intent chatIntent = new Intent(HomeActivity.this, ChatActivity.class);
                chatIntent.putExtra(Constants.USER, user);
                chatIntent.putExtra(Constants.CONNECTION, connection);
                chatIntent.putExtra(Constants.CONNECTION_ID, connectionId);
                chatIntent.putExtra(Constants.VIEW_TYPE, Constants.ACTIVE_CONNECTION_VIEW_TAG);
                startActivity(chatIntent);
                break;
            case Constants.CONNECTION_DECLINED_DIALOG:
                showConnectionDeclinedDialog(this, notificationMessage);
                break;
        }

        isPushNotificationMode = false;
    }

    private void showConnectionDeclinedDialog(Context context, final String notificationMessage) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_connection_declined, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextView mDialogTitle = (TextView) dialogView.findViewById(R.id.dialog_title);
        final TextView mDialogContent = (TextView) dialogView.findViewById(R.id.dialog_content);
        final Button mDismissDialog = (Button) dialogView.findViewById(R.id.dismiss_button);

        if (notificationMessage != null) {
            mDialogContent.setText(notificationMessage);
        }

        mDismissDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void initializeFragment(String viewType, Bundle savedInstanceState) {
        this.viewType = viewType;
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.CREATE_PROFILE_TAG:
                        frag = CreateProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.CONNECTIONS_VIEW_TAG:
                    default:
                        frag = ConnectionsFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ConnectionsFragment.newInstance(fragmentBundle);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    private void styleNavigationMenu(NavigationView navigationView) {
        Menu navigationMenu = navigationView.getMenu();
        for (int i = 0; i < navigationMenu.size(); i++) {
            MenuItem menuItem = navigationMenu.getItem(i);
            SubMenu subNavigationMenu = menuItem.getSubMenu();
            if (subNavigationMenu != null && subNavigationMenu.size() > 0) {
                for (int j = 0; j < subNavigationMenu.size(); j++) {
                    MenuItem subMenuItem = subNavigationMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(menuItem);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface typeface = FontUtils.selectTypeface(this, FontUtils.STYLE_BOLD);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void loadUserData(User user) {
        if (user != null) {
            if (user.getFirstName() != null && user.getLastName() != null) {
                String username = user.getFirstName().concat(" ").concat(user.getLastName());
                mUsername.setText(username);
            }

            if (user.getProfilePhotoUrl().contains(getString(R.string.base_url))) {
                String imageKey = user.getProfilePhotoUrl().split("image/")[1];
                if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                    final String imageData = FileUtils.getImageDataFromCache(imageKey);
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                                profilePhoto.setImageBitmap(decodedByte);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.run();
                } else {
                    fetchImageBackgroundTask(imageKey, profilePhoto);
                }
            } else {
                GlideUtils.loadGlideImage(profilePhoto, getApplicationContext(), user.getProfilePhotoUrl());
            }
        }
    }

    private void fetchImageBackgroundTask(final String imageKey, final ImageView profilePhoto) {
        if (NetworkUtils.isConnected(getApplicationContext())) {
            new ApiClient.NetworkCallsRunner(Constants.FETCH_IMAGE_REQUEST, imageKey, new ApiClientListener.FetchEncodedImageListener() {
                @Override
                public void onEncodedImageFetched(ImageResponse imageResponse) {
                    if (imageResponse != null && imageResponse.getBase64ImageString() != null && imageResponse.getBase64ImageString().length() > 0) {
                        FileUtils.saveImageToCache(imageKey, imageResponse.getBase64ImageString());
                        byte[] decodedImageResource = Base64.decode(imageResponse.getBase64ImageString(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
                        profilePhoto.setImageBitmap(decodedByte);
                    }
                }
            }).execute();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_profile:
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.BASIC_PROFILE_VIEW_TAG);
                intent.putExtra(Constants.USER, user);
                startActivity(intent);
                break;
            case R.id.action_logout:
                signUserOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch (id) {
            case R.id.nav_find_mentor:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(HomeActivity.this, MentorActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MENTOR_INTERESTS_VIEW_TAG);
                        intent.putExtra(Constants.USER, user);
                        startActivity(intent);
                    }
                }, 250);
                break;
            case R.id.nav_manage_mentorships:
                initializeFragment(Constants.CONNECTIONS_VIEW_TAG, null);
                break;
            case R.id.nav_home:
                initializeFragment(Constants.WELCOME_VIEW_TAG, null);
                break;
            case R.id.nav_profile:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.BASIC_PROFILE_VIEW_TAG);
                        intent.putExtra(Constants.USER, user);
                        startActivity(intent);
                    }
                }, 250);
                break;
            case R.id.nav_logout:
                signUserOut();
                break;
        }

        return true;
    }

    private void signUserOut() {
        PreferenceStorageManager.resetUser(PentorApplication.getAppInstance().getApplicationContext());
        ApiClient.NetworkCallsRunner.resetApiService();
        ApiModule.resetApiClient();

        Intent intent = new Intent(HomeActivity.this, TempActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    @Override
    public void changeView(String viewTag) {
        initializeFragment(viewTag, null);
    }

    @Override
    public void onProfileUpdated(User user) {
        this.user = user;
        loadUserData(user);
    }
}
