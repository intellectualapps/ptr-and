package com.affinislabs.pentor.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.ui.fragments.BioProfileFragment;
import com.affinislabs.pentor.utils.Constants;
import com.bumptech.glide.Glide;


public class SocialPhotoDialog extends BaseDialogFragment implements View.OnClickListener {


    ProgressDialog progressDialog;
    View mSnackBarAnchor;
    String socialPhotoUrl;
    ImageView mSocialPhotoView;
    TextView mTakePhotoButton, mUploadPhotoButton, mUsePhotoButton;
    static BioProfileFragment.SocialPhotoPickerListener socialPhotoPickerListener;

    public static DialogFragment getInstance(Bundle args, BioProfileFragment.SocialPhotoPickerListener socialPhotoPickerListenerParam) {
        SocialPhotoDialog frag = new SocialPhotoDialog();

        frag.setArguments(args);
        socialPhotoPickerListener = socialPhotoPickerListenerParam;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.SOCIAL_PHOTO_URL)) {
                socialPhotoUrl = getArguments().getString(Constants.SOCIAL_PHOTO_URL);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.social_photo_dialog, container, false);
        mSnackBarAnchor = view;
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        mSocialPhotoView = (ImageView) view.findViewById(R.id.social_photo);
        mTakePhotoButton = (TextView) view.findViewById(R.id.take_photo_button);
        mUploadPhotoButton = (TextView) view.findViewById(R.id.upload_photo_button);
        mUsePhotoButton = (TextView) view.findViewById(R.id.use_image_button);

        mTakePhotoButton.setOnClickListener(this);
        mUsePhotoButton.setOnClickListener(this);
        mUploadPhotoButton.setOnClickListener(this);

        if (socialPhotoUrl != null) {
            Glide.with(getContext()).load(socialPhotoUrl).into(mSocialPhotoView);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.use_image_button:
                socialPhotoPickerListener.onOptionSelected(0);
                break;
            case R.id.take_photo_button:
                socialPhotoPickerListener.onOptionSelected(MediaPickerDialog.TYPE_TAKE_IMAGE);
                break;
            case R.id.upload_photo_button:
                socialPhotoPickerListener.onOptionSelected(MediaPickerDialog.TYPE_PICK_IMAGE);
                break;
        }
        if (isVisible())
            dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
