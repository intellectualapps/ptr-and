package com.affinislabs.pentor.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.ui.fragments.BioProfileFragment;
import com.affinislabs.pentor.ui.fragments.ConnectionsFragment;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FirebaseUtils;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


public class EndConnectionDialog extends BaseDialogFragment implements View.OnClickListener, RatingBar.OnRatingBarChangeListener {


    private ProgressDialog progressDialog;
    private View mSnackBarAnchor;
    private String socialPhotoUrl;
    private ImageView mSocialPhotoView;
    private Button mCancelDialogButton, mRemoveButton;
    private String connectionId;
    private RatingBar ratingBar;
    private String rating = "0", emailAddress;


    public static DialogFragment getInstance(Bundle args) {
        EndConnectionDialog frag = new EndConnectionDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.CONNECTION_ID)) {
                connectionId = getArguments().getString(Constants.CONNECTION_ID);
            }

            if (getArguments().containsKey(Constants.EMAIL_ADDRESS)) {
                emailAddress = getArguments().getString(Constants.EMAIL_ADDRESS);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.end_connection_dialog, container, false);
        mSnackBarAnchor = view;
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        mSocialPhotoView = (ImageView) view.findViewById(R.id.social_photo);
        mRemoveButton = (Button) view.findViewById(R.id.remove_button);
        mCancelDialogButton = (Button) view.findViewById(R.id.cancel_button);
        ratingBar = (RatingBar) view.findViewById(R.id.rating_bar);

        ratingBar.setOnRatingBarChangeListener(this);
        ratingBar.setNumStars(5);

        mCancelDialogButton.setOnClickListener(this);
        mRemoveButton.setOnClickListener(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                if (isVisible())
                    dismiss();
                break;
            case R.id.remove_button:
                sendRatingRequest();
                break;
        }
    }

    private void sendRatingRequest() {
        showLoadingIndicator(true, getString(R.string.end_connection_progress));
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(Constants.CONNECTION_ID, connectionId);
        requestMap.put(Constants.RATING, rating);
        requestMap.put(Constants.USER_EMAIL, emailAddress);
        ApiClient.NetworkCallsRunner.rateUser(requestMap, null);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ConnectionsFragment.refreshConnectionsListener != null) {
                    ConnectionsFragment.refreshConnectionsListener.onConnectionRequestUpdated();
                }
                showLoadingIndicator(false, getString(R.string.end_connection_progress));
                dismiss();
                getActivity().finish();
            }
        }, 3000);
    }

    private void deleteConnectionOnFirebase() {
        DatabaseReference connectionDatabaseReference = FirebaseUtils.getConnectionsDatabaseReference().child(connectionId);
        connectionDatabaseReference.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    dismiss();
                    getActivity().finish();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        this.rating = String.valueOf(rating);
    }
}
