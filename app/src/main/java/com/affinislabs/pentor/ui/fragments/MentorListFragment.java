package com.affinislabs.pentor.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.MentorConnectionResponse;
import com.affinislabs.pentor.api.responses.MentorResponse;
import com.affinislabs.pentor.interfaces.PentorInterfaces;
import com.affinislabs.pentor.models.SwipeObject;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.activities.MentorActivity;
import com.affinislabs.pentor.ui.adapters.MentorListAdapter;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.listeners.ItemRemovedListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MentorListFragment extends BaseFragment implements View.OnClickListener, PentorInterfaces.MentorClickListener {
    private Toolbar toolbar;
    private ViewPager viewPager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private MentorListAdapter mAdapter;
    private ArrayList<User> mMentorList;
    private Map<String, String> dataMap;
    private String selectedSubInterestId;
    private View mSnackBarView, mEmptyView;
    private TextView emptyViewLabel;
    private User user;
    private AlertDialog alertDialog;
    private SwipePlaceHolderView swipePlaceHolderView;
    private boolean inAcceptMode = false;
    private String message;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MentorListFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public MentorListFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataMap = new HashMap<String, String>();
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.MENTOR_INTERESTS) && getArguments().get(Constants.MENTOR_INTERESTS) != null) {
                selectedSubInterestId = getArguments().getString(Constants.MENTOR_INTERESTS);
                dataMap.put(Constants.SUB_INTEREST_ID, selectedSubInterestId);
            } else {
                closeFragment();
            }

            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
                dataMap.put(Constants.EMAIL_ADDRESS, user != null ? user.getEmail() : null);
            }
        }
        message = PentorApplication.getApplicationResources().getString(R.string.fetch_mentors);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mentor_list, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((MentorActivity) getActivity()).getToolbar();
        toolbar.setTitle(PentorApplication.getApplicationResources().getString(R.string.mentors_list_label));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MentorActivity) getActivity()).navigateUp();
            }
        });

        swipePlaceHolderView = (SwipePlaceHolderView) view.findViewById(R.id.swipe_view);
        mEmptyView = view.findViewById(R.id.empty_view);
        emptyViewLabel = (TextView) mEmptyView.findViewById(R.id.empty_view_label);

        emptyViewLabel.setText(message);

        swipePlaceHolderView.disableTouchSwipe();
        swipePlaceHolderView.addItemRemoveListener(new ItemRemovedListener() {
            @Override
            public void onItemRemoved(int count) {
                if (count == 0 && !inAcceptMode) {
                    closeView();
                }
            }
        });

        fetchMentorList(dataMap);

        SwipeDecor swipeDecor = new SwipeDecor().setPaddingTop(50)
                .setRelativeScale(0.01f)
                .setSwipeInMsgLayoutId(R.layout.swipe_in_msg_view)
                .setSwipeOutMsgLayoutId(R.layout.swipe_out_msg_view);
        //.setMarginTop(300)
        //.setMarginLeft(100)
        //.setViewGravity(Gravity.TOP);

        swipePlaceHolderView.getBuilder()
                //.setSwipeType(SwipePlaceHolderView.SWIPE_TYPE_VERTICAL)
                .setDisplayViewCount(5)
                .setIsUndoEnabled(false)
                .setWidthSwipeDistFactor(15)
                .setHeightSwipeDistFactor(20)
                .setSwipeDecor(swipeDecor);


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    swipePlaceHolderView.enableTouchSwipe();
                    /*swipePlaceHolderView.lockViews();
                    Thread.currentThread().sleep(4000);
                    swipePlaceHolderView.unlockViews();
                    Thread.currentThread().sleep(4000);*/
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void closeView() {
        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.mentor_match_label));
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Thread.sleep(2000);
                    getActivity().finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private void fetchMentorList(Map<String, String> dataMap) {
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(true, message);
            new ApiClient.NetworkCallsRunner(Constants.FETCH_MENTORS_REQUEST, dataMap, new ApiClientListener.FetchMentorsListener() {
                @Override
                public void onMentorsFetched(MentorResponse mentorResponse) {
                    if (mentorResponse != null && mentorResponse.getMessage() == null) {
                        mMentorList = mentorResponse.getMentors();
                        if (mMentorList != null && mMentorList.size() > 0) {
                            for (User user : mMentorList) {
                                swipePlaceHolderView.addView(new SwipeObject(user, MentorListFragment.this, getContext()));
                            }
                            mEmptyView.setVisibility(View.GONE);
                            swipePlaceHolderView.setVisibility(View.VISIBLE);
                        } else {
                            mEmptyView.setVisibility(View.VISIBLE);
                            emptyViewLabel.setText(R.string.no_mentors_label);
                            swipePlaceHolderView.setVisibility(View.GONE);
                        }
                    }
                    showLoadingIndicator(false, message);
                }
            }).execute();
        } else {
            CommonUtils.displaySnackBarMessage(mSnackBarView, PentorApplication.getApplicationResources().getString(R.string.network_connection_label));
        }
    }

    private void showInitialMessageDialog(final Context context, final User userParam) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.mentor_request_dialog, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        alertDialog = builder.create();

        final TextView mRequestLabel = (TextView) dialogView.findViewById(R.id.request_label);
        final TextView mCancelRequestView = (TextView) dialogView.findViewById(R.id.cancel_button);
        final TextView mSendRequestView = (TextView) dialogView.findViewById(R.id.send_request_button);
        final EditText mRequestMessageEditText = (EditText) dialogView.findViewById(R.id.request_message);

        mSendRequestView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRequestMessageEditText.getText() != null && mRequestMessageEditText.length() > 0) {
                    String initialMessage = mRequestMessageEditText.getText().toString();
                    dataMap.put(Constants.INITIAL_MESSAGE, initialMessage);
                }
                dataMap.put(Constants.MENTEE_EMAIL, user.getEmail());
                dataMap.put(Constants.MENTOR_EMAIL, userParam.getEmail());
                dataMap.put(Constants.SUB_INTEREST_ID, selectedSubInterestId);

                establishMentorConnection(dataMap, context);
            }
        });

        mCancelRequestView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                swipePlaceHolderView.undoLastSwipe();
                /*if (swipePlaceHolderView.getChildCount() == 0) {
                    closeView();
                }*/
            }
        });

        alertDialog.show();
    }

    private void establishMentorConnection(Map<String, String> dataMap, Context context) {
        final String message = PentorApplication.getApplicationResources().getString(R.string.connection_request_loading);
        if (NetworkUtils.isConnected(context)) {
            alertDialog.hide();
            showLoadingIndicator(true, message);
            new ApiClient.NetworkCallsRunner(Constants.ESTABLISH_CONNECTION_REQUEST, dataMap, new ApiClientListener.MentorConnectionListener() {
                @Override
                public void onConnectionEstablished(MentorConnectionResponse mentorConnectionResponse) {
                    showLoadingIndicator(false, message);
                    if (mentorConnectionResponse != null && mentorConnectionResponse.getMessage() == null) {
                        alertDialog.dismiss();
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {
                                CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.mentor_connection_success));
                                try {
                                    Thread.sleep(2000);
                                    getActivity().finish();
                                    HomeActivity.changeHomeActivityViewListener.changeView(Constants.CONNECTIONS_VIEW_TAG);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }.execute();
                    } else {
                        alertDialog.show();
                        if (mentorConnectionResponse != null) {
                            CommonUtils.displaySnackBarMessage(mSnackBarView, mentorConnectionResponse.getMessage());
                        }
                    }
                }
            }).execute();
        } else {
            CommonUtils.displaySnackBarMessage(mSnackBarView, PentorApplication.getApplicationResources().getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onMentorSkipClicked(User user, int currentIndex) {
        inAcceptMode = false;
        System.out.println(user.getFullName() + " was skipped");
    }

    @Override
    public void onMentorAddClicked(User userParam, int currentIndex) {
        inAcceptMode = true;
        if (userParam != null) {
            showInitialMessageDialog(getContext(), userParam);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_mentor:
                swipePlaceHolderView.undoLastSwipe();
                System.out.println(swipePlaceHolderView.getAllResolvers().size() + " resolvers");
                break;
            case R.id.skip_mentor:
                swipePlaceHolderView.doSwipe(false);
                break;
        }
    }
}
