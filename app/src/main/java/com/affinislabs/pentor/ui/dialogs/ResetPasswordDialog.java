package com.affinislabs.pentor.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.ApiModule;
import com.affinislabs.pentor.api.responses.LoginResponse;
import com.affinislabs.pentor.api.responses.UserProfileResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.fragments.ConnectionsFragment;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FirebaseUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


public class ResetPasswordDialog extends BaseDialogFragment implements View.OnClickListener, TextWatcher {


    private View mSnackBarAnchor;
    private EditText emailAddressInput;
    private Button mCancelDialogButton, mResetPasswordButton;
    private String connectionId;
    private RatingBar ratingBar;
    private String emailAddress;


    public static DialogFragment getInstance(Bundle args) {
        ResetPasswordDialog frag = new ResetPasswordDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.EMAIL_ADDRESS)) {
                emailAddress = getArguments().getString(Constants.EMAIL_ADDRESS);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.reset_password_dialog, container, false);
        mSnackBarAnchor = view;
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        emailAddressInput = (EditText) view.findViewById(R.id.email_address);
        mResetPasswordButton = (Button) view.findViewById(R.id.reset_button);
        mCancelDialogButton = (Button) view.findViewById(R.id.cancel_button);

        emailAddressInput.addTextChangedListener(this);

        mCancelDialogButton.setOnClickListener(this);
        mResetPasswordButton.setOnClickListener(this);
        if (emailAddress != null && emailAddress.trim().length() > 0) {
            emailAddressInput.setText(emailAddress);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                if (isVisible()) {
                    dismiss();
                }
                break;
            case R.id.reset_button:
                hideKeyboard();
                if (NetworkUtils.isConnected(getContext())) {
                    if (validateFields(new EditText[]{emailAddressInput})) {
                        String emailAddress = emailAddressInput.getText().toString();
                        sendResetRequest(emailAddress);
                    }
                } else {
                    CommonUtils.displayToastMessage(getContext(), getString(R.string.network_connection_error));
                }
                break;
        }
    }

    private void sendResetRequest(String emailAddress) {
        showLoadingIndicator(true, getString(R.string.reset_password_loading));
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(Constants.EMAIL_ADDRESS, emailAddress);
        new ApiClient.NetworkCallsRunner(Constants.RESET_PASSWORD_REQUEST, requestMap, new ApiClientListener.ResetPasswordListener() {
            @Override
            public void onPasswordReset(UserProfileResponse userProfileResponse) {
                showLoadingIndicator(false, "");
                if (userProfileResponse != null) {
                    if (userProfileResponse.getMessage() == null) {
                        CommonUtils.displayToastMessage(getContext(), getString(R.string.password_reset_successful_message));
                        if (isVisible()) {
                            dismiss();
                        }
                    } else {
                        CommonUtils.displayToastMessage(getContext(), userProfileResponse.getMessage());
                    }
                } else {
                    if (isVisible()) {
                        dismiss();
                    }
                    CommonUtils.displayToastMessage(getContext(), getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
