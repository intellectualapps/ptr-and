package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.Connection;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.dialogs.MediaPickerDialog;
import com.affinislabs.pentor.ui.fragments.ActiveConnectionFragment;
import com.affinislabs.pentor.ui.fragments.ConnectionsFragment;
import com.affinislabs.pentor.ui.fragments.MediaPickerBaseFragment;
import com.affinislabs.pentor.ui.fragments.MenteeRequestFragment;
import com.affinislabs.pentor.ui.fragments.MentorInterestsFragment;
import com.affinislabs.pentor.ui.fragments.MentorListFragment;
import com.affinislabs.pentor.ui.fragments.MentorRequestFragment;
import com.affinislabs.pentor.utils.Constants;

import java.io.Serializable;
import java.util.Map;

public class ConnectionsActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    Bundle fragmentBundle;
    String viewType;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.connections));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            viewType = intent.getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = intent.getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.MENTOR_REQUEST_VIEW_TAG:
                        frag = MentorRequestFragment.newInstance(fragmentBundle);
                        break;
                    default:
                    case Constants.MENTEE_REQUEST_VIEW_TAG:
                        frag = MenteeRequestFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ActiveConnectionFragment.newInstance(fragmentBundle);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, int mediaType, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, mediaType, fragmentTag);
            }
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
            }
        }
    }
}
