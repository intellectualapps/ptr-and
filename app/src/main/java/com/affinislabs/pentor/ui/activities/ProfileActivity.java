package com.affinislabs.pentor.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.dialogs.MediaPickerDialog;
import com.affinislabs.pentor.ui.fragments.BasicProfileFragment;
import com.affinislabs.pentor.ui.fragments.BioProfileFragment;
import com.affinislabs.pentor.ui.fragments.MediaPickerBaseFragment;
import com.affinislabs.pentor.utils.Constants;

import java.io.Serializable;
import java.util.Map;

public class ProfileActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    Bundle fragmentBundle;
    String viewType;
    User user;
    Toolbar toolbar;
    Map<String, String> basicProfileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.profile_label));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            viewType = intent.getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = new Bundle();

            user = intent.getExtras().getParcelable(Constants.USER);
            basicProfileData = (Map<String, String>) intent.getExtras().getSerializable(Constants.BASIC_PROFILE_DATA);

            fragmentBundle.putParcelable(Constants.USER, user);
            fragmentBundle.putSerializable(Constants.BASIC_PROFILE_DATA, (Serializable) basicProfileData);
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.BASIC_PROFILE_VIEW_TAG:
                        frag = BasicProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.BIO_PROFILE_VIEW_TAG:
                        frag = BioProfileFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = BasicProfileFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = BasicProfileFragment.newInstance(fragmentBundle);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, int mediaType, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, mediaType, fragmentTag);
            }
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        if (frag != null) {
            //check if the fragment is an instance of MediaPickerBaseFragment
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
            }
        }
    }
}
