package com.affinislabs.pentor.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.ui.fragments.ConnectionsFragment;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FirebaseUtils;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


public class BlockConnectionDialog extends BaseDialogFragment implements View.OnClickListener, ApiClientListener.BlockUserListener {


    private ProgressDialog progressDialog;
    private View mSnackBarAnchor;
    private Button mCancelDialogButton, mBlockUserButton;
    private String connectionId;
    private String userEmail;
    private String blockEmail;


    public static DialogFragment getInstance(Bundle args) {
        BlockConnectionDialog frag = new BlockConnectionDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.CONNECTION_ID)) {
                connectionId = getArguments().getString(Constants.CONNECTION_ID);
            }

            if (getArguments().containsKey(Constants.USER_EMAIL)) {
                userEmail = getArguments().getString(Constants.USER_EMAIL);
            }

            if (getArguments().containsKey(Constants.BLOCK_EMAIL_ADDRESS)) {
                blockEmail = getArguments().getString(Constants.BLOCK_EMAIL_ADDRESS);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.block_connection_dialog, container, false);
        mSnackBarAnchor = view;
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        mBlockUserButton = (Button) view.findViewById(R.id.block_user_button);
        mCancelDialogButton = (Button) view.findViewById(R.id.cancel_button);

        mCancelDialogButton.setOnClickListener(this);
        mBlockUserButton.setOnClickListener(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                if (isVisible())
                    dismiss();
                break;
            case R.id.block_user_button:
                sendBlockUserRequest();
                break;
        }
    }

    private void sendBlockUserRequest() {
        showLoadingIndicator(true, getString(R.string.block_user_progress));
        ApiClient.NetworkCallsRunner.blockUser(userEmail, blockEmail, connectionId, this);
    }

    private void deleteConnectionOnFirebase() {
        DatabaseReference connectionDatabaseReference = FirebaseUtils.getConnectionsDatabaseReference().child(connectionId);
        connectionDatabaseReference.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    dismiss();
                    getActivity().finish();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onUserBlocked() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ConnectionsFragment.refreshConnectionsListener != null) {
                    ConnectionsFragment.refreshConnectionsListener.onConnectionRequestUpdated();
                }
                showLoadingIndicator(false, getString(R.string.end_connection_progress));
                dismiss();
                getActivity().finish();
            }
        }, 3000);
    }
}
