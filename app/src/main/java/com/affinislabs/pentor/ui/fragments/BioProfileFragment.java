package com.affinislabs.pentor.ui.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.affinislabs.pentor.PentorApplication;
import com.affinislabs.pentor.R;
import com.affinislabs.pentor.api.ApiClient;
import com.affinislabs.pentor.api.ApiClientListener;
import com.affinislabs.pentor.api.responses.ImageResponse;
import com.affinislabs.pentor.api.responses.ProfileUpdateResponse;
import com.affinislabs.pentor.models.User;
import com.affinislabs.pentor.ui.activities.HomeActivity;
import com.affinislabs.pentor.ui.activities.ProfileActivity;
import com.affinislabs.pentor.ui.customviews.TextView;
import com.affinislabs.pentor.ui.dialogs.MediaPickerDialog;
import com.affinislabs.pentor.ui.dialogs.SocialPhotoDialog;
import com.affinislabs.pentor.utils.CommonUtils;
import com.affinislabs.pentor.utils.Constants;
import com.affinislabs.pentor.utils.FileUtils;
import com.affinislabs.pentor.utils.GlideUtils;
import com.affinislabs.pentor.utils.NetworkUtils;
import com.affinislabs.pentor.utils.PreferenceStorageManager;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class BioProfileFragment extends MediaPickerBaseFragment implements View.OnClickListener, View.OnFocusChangeListener, PlaceSelectionListener {
    private static final String TAG = BioProfileFragment.class.getSimpleName();
    private static final int REQUEST_SELECT_PLACE = 101;
    Toolbar toolbar;
    View mSnackBarView;
    User user;
    EditText mBioEditText, mLocationEditText, mAboutEditText;
    TextView mSubmitView, mSelectPhoto;
    ImageView mProfilePhoto;
    Uri mFileUri;
    String profilePhotoUrl;
    boolean shouldUseSocialPhoto = false;
    boolean shouldShowSocialDialog = false;
    LatLng addressLocation;
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 100;
    String message;

    private Map<String, String> basicProfileData;
    Map<String, String> socialProfile = new HashMap<String, String>();
    private boolean showImagePickerDialogAfterPermissionGranted = false;
    SocialPhotoPickerListener socialPhotoPickerListener;
    DialogFragment socialPhotoDialogFragment;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new BioProfileFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public BioProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }

            if (getArguments().containsKey(Constants.BASIC_PROFILE_DATA)) {
                basicProfileData = (Map<String, String>) getArguments().getSerializable(Constants.BASIC_PROFILE_DATA);
            }
        }

        socialProfile = PreferenceStorageManager.getSocialProfileData(PentorApplication.getAppInstance().getApplicationContext());
        socialPhotoPickerListener = new SocialPhotoPickerListener() {
            @Override
            public void onOptionSelected(int option) {
                if (socialPhotoDialogFragment != null && socialPhotoDialogFragment.isVisible()) {
                    socialPhotoDialogFragment.dismiss();
                }
                switch (option) {
                    case 0:
                        mSelectPhoto.setText(getString(R.string.change_photo_label));
                        shouldUseSocialPhoto = true;
                        Glide.with(getContext()).load(profilePhotoUrl).into(mProfilePhoto);
                        break;
                    case MediaPickerDialog.TYPE_TAKE_IMAGE:
                        launchImagePickerDialog(MediaPickerDialog.TYPE_TAKE_IMAGE);
                        break;
                    case MediaPickerDialog.TYPE_PICK_IMAGE:
                        launchImagePickerDialog(MediaPickerDialog.TYPE_PICK_IMAGE);
                        break;
                }
            }
        };
        message = getString(R.string.update_progress_label);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_bio, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        resolveImageView(showImagePickerDialogAfterPermissionGranted);
    }


    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((ProfileActivity) getActivity()).getToolbar();
        toolbar.setTitle(getString(R.string.bio_label));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity) getActivity()).navigateUp();
            }
        });
        mSubmitView = (TextView) view.findViewById(R.id.submit_button);
        mAboutEditText = (EditText) view.findViewById(R.id.about);
        mBioEditText = (EditText) view.findViewById(R.id.bio);
        mLocationEditText = (EditText) view.findViewById(R.id.location);
        mProfilePhoto = (ImageView) view.findViewById(R.id.profile_photo);
        mSelectPhoto = (TextView) view.findViewById(R.id.add_photo_button);

        mSubmitView.setOnClickListener(this);
        mSelectPhoto.setOnClickListener(this);
        mLocationEditText.setOnClickListener(this);
        mLocationEditText.setOnFocusChangeListener(this);

        checkIfSocialProfilePhotoExists();
        loadUserData(user);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                showImagePickerDialogAfterPermissionGranted = grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_button:
                if (validateInputLength(mAboutEditText, 140) && validateInputLength(mBioEditText, 2000) && validateFields(new EditText[]{mLocationEditText})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        showLoadingIndicator(true, message);
                        makeAPICall(getInput());
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.validation_prompt_label));
                }
                break;
            case R.id.location:
                if (NetworkUtils.isConnected(getContext())) {
                    launchAutocompleteView();
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                }
                break;
            case R.id.add_photo_button:
                if (checkFileAccessPermission()) {
                    if (shouldShowSocialDialog && profilePhotoUrl != null && profilePhotoUrl.length() > 0) {
                        launchSocialPhotoDialog(profilePhotoUrl);
                    } else {
                        launchImagePickerDialog(MediaPickerDialog.TYPE_IMAGE);
                    }
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.location:
                if (hasFocus) {
                    if (NetworkUtils.isConnected(getContext())) {
                        launchAutocompleteView();
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                    }
                }
                break;
        }
    }

    public Map<String, String> getInput() {
        Map<String, String> map = new HashMap<String, String>();
        map = basicProfileData;
        String bio = mBioEditText.getText().toString().trim();
        String location = mLocationEditText.getText().toString().trim();
        String about = mAboutEditText.getText().toString().trim();

        map.put(Constants.ABOUT, about);
        map.put(Constants.BIO, bio);
        map.put(Constants.EMAIL_ADDRESS, user.getEmail());
        map.put(Constants.LOCATION, location);

        if (addressLocation != null) {
            map.put(Constants.LONGITUDE, String.valueOf(addressLocation.longitude));
            map.put(Constants.LATITUDE, String.valueOf(addressLocation.latitude));
        }

        map.put(Constants.IMAGE_KEY, "pp");
        if (mFileUri != null) {
            String base64Value = FileUtils.getBase64Representation(mFileUri.getPath());
            map.put(Constants.IMAGE_DATA, base64Value);
        } else {
            if (profilePhotoUrl != null && shouldUseSocialPhoto) {
                map.put(Constants.SOCIAL_PHOTO_URL, profilePhotoUrl);
            }
        }

        return map;
    }

    public Map<String, String> sanitizeInput(Map<String, String> input) {
        Map<String, String> sanitizedMap = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : input.entrySet()) {
            if (entry.getKey() != null) {
                if (entry.getValue() != null && entry.getValue().length() > 0) {
                    sanitizedMap.put(entry.getKey(), entry.getValue());
                } else {
                    sanitizedMap.remove(entry.getKey());
                }
            }
        }
        return sanitizedMap;
    }

    private void resolveImageView(boolean showImagePickerDialogParam) {
        if (showImagePickerDialogParam) {
            if (shouldShowSocialDialog && profilePhotoUrl != null && profilePhotoUrl.length() > 0) {
                launchSocialPhotoDialog(profilePhotoUrl);
            } else {
                launchImagePickerDialog(MediaPickerDialog.TYPE_IMAGE);
            }
            showImagePickerDialogAfterPermissionGranted = false;
        }
    }

    private void loadUserData(User user) {

        if (user != null) {
            if (user.getProfilePhotoUrl() != null && user.getProfilePhotoUrl().length() > 2) {
                mSelectPhoto.setText(getString(R.string.change_photo_label));
                if (user.getProfilePhotoUrl().contains(getString(R.string.base_url))) {
                    String imageKey = user.getProfilePhotoUrl().split("image/")[1];
                    if (FileUtils.checkIfImageAlreadyCached(imageKey)) {
                        loadImageData(imageKey, mProfilePhoto);
                    } else {
                        fetchImageBackgroundTask(imageKey, mProfilePhoto, getContext());
                    }
                } else {
                    GlideUtils.loadGlideImage(mProfilePhoto, getContext(), user.getProfilePhotoUrl());
                }
            }

            if (user.getShortBio() != null) {
                mAboutEditText.setText(user.getShortBio());
            }

            if (user.getLongBio() != null) {
                mBioEditText.setText(user.getLongBio());
            }

            if (user.getAddress() != null) {
                mLocationEditText.setText(user.getAddress());
            }
        }
    }

    private void checkIfSocialProfilePhotoExists() {
        if (socialProfile != null && socialProfile.size() > 0) {
            if (socialProfile.containsKey(Constants.PROFILE_PHOTO) && socialProfile.get(Constants.PROFILE_PHOTO) != null) {
                profilePhotoUrl = socialProfile.get(Constants.PROFILE_PHOTO);
                shouldShowSocialDialog = true;
            }
        }
    }

    private void launchImagePickerDialog(int mediaPickerViewType) {
        DialogFragment profileBannerFrag = MediaPickerDialog.newInstance(getString(R.string.set_profile_photo_label), mediaPickerViewType, BioProfileFragment.class.getSimpleName());
        FragmentTransaction fragmentTransaction = ((AppCompatActivity) getContext()).getSupportFragmentManager().beginTransaction();
        profileBannerFrag.show(fragmentTransaction, MediaPickerDialog.class.getSimpleName());
    }

    private void launchSocialPhotoDialog(String socialPhotoUrl) {
        Bundle args = new Bundle();
        args.putString(Constants.SOCIAL_PHOTO_URL, socialPhotoUrl);
        socialPhotoDialogFragment = SocialPhotoDialog.getInstance(args, socialPhotoPickerListener);
        if (getActivity() != null)
            socialPhotoDialogFragment.show(getActivity().getSupportFragmentManager(), socialPhotoDialogFragment.getClass().getSimpleName());
    }

    public interface SocialPhotoPickerListener {
        void onOptionSelected(int option);
    }

    private boolean checkFileAccessPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPDATE_PROFILE_REQUEST, accountMap, new ApiClientListener.ProfileUpdatedListener() {
            @Override
            public void onProfileUpdated(ProfileUpdateResponse profileUpdateResponse) {
                showLoadingIndicator(false, message);
                if (profileUpdateResponse != null) {
                    if (profileUpdateResponse.getStatus() == null && profileUpdateResponse.getMessage() == null) {
                        Bundle bundle = new Bundle();
                        User user = extractUserDetails(profileUpdateResponse);

                        PreferenceStorageManager.setProfileUpdateStatus(PentorApplication.getAppInstance().getApplicationContext(), true);
                        PreferenceStorageManager.saveUser(PentorApplication.getAppInstance().getApplicationContext(), user);
                        bundle.putParcelable(Constants.USER, (Parcelable) user);

                        if (HomeActivity.userProfileUpdateListener != null) {
                            HomeActivity.userProfileUpdateListener.onProfileUpdated(user);
                        }

                        if (HomeFragment.userProfileUpdateListener != null) {
                            HomeFragment.userProfileUpdateListener.onProfileUpdated(user);
                        }

                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.profile_update_success_label));

                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                try {
                                    Thread.sleep(1000);
                                    HomeActivity.changeHomeActivityViewListener.changeView(Constants.CONNECTIONS_VIEW_TAG);
                                    getActivity().finish();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (NullPointerException npe) {
                                    npe.printStackTrace();
                                }
                                return null;
                            }
                        }.execute();

                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, profileUpdateResponse.getMessage());
                    }
                } else {
                    CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private User extractUserDetails(ProfileUpdateResponse profileUpdateResponse) {
        User user = new User();
        user.setEmail(profileUpdateResponse.getEmail());
        user.setAuthToken(profileUpdateResponse.getAuthToken());
        user.setFirstName(profileUpdateResponse.getFirstName());
        user.setLastName(profileUpdateResponse.getLastName());
        user.setProfilePhotoUrl(profileUpdateResponse.getProfilePhotoUrl());
        user.setShortBio(profileUpdateResponse.getShortBio());
        user.setLongBio(profileUpdateResponse.getLongBio());
        user.setEmailAddressVerified(profileUpdateResponse.isEmailAddressVerified());
        user.setPhoneNumber(profileUpdateResponse.getPhoneNumber());
        user.setGender(profileUpdateResponse.getGender());
        user.setBirthday(profileUpdateResponse.getBirthday());
        user.setMentor(profileUpdateResponse.isMentor());
        user.setLongitude(profileUpdateResponse.getLongitude());
        user.setLatitude(profileUpdateResponse.getLatitude());
        user.setAddress(profileUpdateResponse.getAddress());
        user.setUserRoles(profileUpdateResponse.getUserRoles());
        user.setMentorInterests(profileUpdateResponse.getMentorInterests());
        return user;
    }

    @Override
    public void onMediaPickerSuccess(Uri fileUri, int mediaType, String fragmentTag) {
        try {
            if (!fileUri.toString().contains("content://")) {
                setImagePreview(fileUri);
            } else {
                Uri resolvedUri = Uri.parse("file://" + FileUtils.getPath(fileUri, PentorApplication.getAppInstance().getApplicationContext()));
                setImagePreview(resolvedUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setImagePreview(Uri fileUri) {
        mSelectPhoto.setText(R.string.change_photo_label);
        shouldUseSocialPhoto = false;
        this.mFileUri = fileUri;
        Glide.with(getContext()).load(fileUri).into(mProfilePhoto);
    }

    @Override
    public void onMediaPickerError(String message, String fragmentTag) {
        if (getActivity() != null) {
            CommonUtils.displaySnackBarMessage(mSnackBarView, message);
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        String resolvedLocation = place.getName() + "," + place.getAddress();
        place.getLatLng();
        addressLocation = place.getLatLng();
        mLocationEditText.setText(resolvedLocation);
    }

    @Override
    public void onError(Status status) {
        CommonUtils.displaySnackBarMessage(mSnackBarView, status.getStatusMessage());
    }

    private void launchAutocompleteView() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder
                    (PlaceAutocomplete.MODE_OVERLAY)
                    .build(getActivity());
            startActivityForResult(intent, REQUEST_SELECT_PLACE);
        } catch (GooglePlayServicesRepairableException |
                GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }
    }
}
